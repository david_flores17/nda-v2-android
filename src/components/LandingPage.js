import React, {Component} from 'react';
import  {
    View,
    Image,
    ActivityIndicator,
    AsyncStorage,
    Text,
    TouchableHighlight,
    Linking
} from 'react-native';
import Globals from './Globals.js'
import styleCss from '../config/css.js'
export default class LandingPage extends Component{
navigate(routeName){
    this.props.navigator.push({
        name:routeName
    })
}
constructor(props){
    super(props);
    this.state = {
        accessToken: "",
        fetching: true,
        }
    }    
    linkToWebsite(url){
        try{
            Linking.openURL(url);  
        }catch(e){
            console.log(e)
        }
    };
    linkToView(route){
        this.navigate(route)
    };

    render(){
        return(
            <View style={styleCss.mainViewContainer}>
                <View style={styleCss.landingLogoContainer}>  
                    <Image
                        style={styleCss.landingLogo}
                        source={require('../images/_white.png')}
                    /> 
                </View>
                <View style={styleCss.landingTextContainer}>
                    <Text style={styleCss.subTitle}>Start doing business faster.</Text>    
                    <Text style={styleCss.descriptionText}>NDA NOW gives you the speed, security, and control you need to power your team. Flexible pricing, scales with you to fit your business needs, no matter how big or small.</Text>    
                </View>
                <View style={styleCss.registerButtonsContainer}>
                    <TouchableHighlight onPress ={()=> this.linkToView('LoginForm')} style={styleCss.registerLoginButton}>
                        <Text style={styleCss.buttonText}>LOGIN</Text>
                    </TouchableHighlight>
                    <TouchableHighlight onPress={ ()=> this.linkToView('RegisterForm')} style={styleCss.registerButton}>
                        <Text style={styleCss.registerButtonText}>REGISTER</Text>
                    </TouchableHighlight>
                </View>
                <View style={styleCss.registerLinkTextContainer}>
                    <TouchableHighlight onPress={()=>this.linkToWebsite(Globals.ndaMainWebpageLink)}>  
                        <Text style={styleCss.linkText}>Visit Our Website</Text>
                    </TouchableHighlight>
                </View>
            </View>   
        );
    }
}


