import React, {Component} from 'react'; 
import {TabNavigator,StackNavigator,Navigate,Navigator} from 'react-navigation';
import { Avatar, Icon} from 'react-native-elements';
import {
    StyleSheet,
    ScrollView,
    TouchableHighlight,
    Text,
    AsyncStorage,
    Image,
    View,
    RefreshControl,
    FlatList,
    Alert
} from 'react-native';
import { SwipeListView } from 'react-native-swipe-list-view';
import moment from 'moment'
import RNRestart from 'react-native-restart';
import api from '../config/api'; 
import Globals from './Globals.js';
import styleCss from '../config/css.js';
const ACCESS_TOKEN = 'access_token';
const USER_ID = 'user_id';
function restart(){
    Alert.alert(
        'Sign out?',
        "Are you sure you want to sign out?",[
        {text: 'OK', onPress: async () => (AsyncStorage.removeItem(ACCESS_TOKEN), AsyncStorage.removeItem(USER_ID),RNRestart.Restart())},//BackAndroid.exitApp(),
        {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
    ]);    
}
export default class Account extends Component{ 
    constructor(props){
        super(props);
        this.state = {
            isRefreshing:false  ,
            user:[],   
            companies:[], 
            company:"",
            plan:[],
            limitInfo:[],
            startMonth :'',
            endMonth :'',    
            notificationsList:[],        
            months : ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'Novemeber', 'December']  ,            
       }
        setInterval(() => {
            //console.log('44 Checking for Account updates should?:',Globals.updateNdaAccount)
            if(Globals.updateNdaAccount){
                console.log('45 UPDATING XXXXXXXXX')
                this.getAccount()
                //this.setState({loaded:false });
                Globals.updateNdaAccount=false
                console.log('uploaded succefully')
            }else{
                //console.log('53 nothing to update')
            }
        }, 5000); 
    } 
    async getAccount(){
        this.setState({isRefreshing: true })
        await api.getUser(this.accessToken).then((res)=>{
            console.log(res)
            if(res.status){
                this.setState({
                    user:res.data.user,
                    companies:res.data.companies[0],
                    company:res.data.company,
                })
                Globals.updateNdaAccount=false
            }else{
                alert('This action failed!, Please check your internet connection!')
            }
            api.pick_Company(this.state.companies.id).then((res)=>{
                if(res.status){        
                    this.setState({
                        plan:res.data.plan,
                        limitInfo:res.data.invite_limit_info,
                        startMonth:res.data.invite_limit_info.month_start_date,
                        endMonth:res.data.invite_limit_info.month_end_date,    
                    })        
                }else{
                    alert('This action failed!, Please check your internet connection!')
                }
            }) 
            api.getNotifications().then((res)=>{
                if(res.status){        
                    this.setState({
                        notificationsList : res.data.list,
                        isRefreshing : false      
                    })        
                }else{
                    alert('This action failed!, Please check your internet connection!')
                }
            }) 
        }) 
    }
    renderNotifications(item){
        switch(item.type) {
            case 'sent':
                if(item.read == 0){
                    var tempNotificationIconName = <Image style={styleCss.detailsBoltOnsImage} source={require( "../images/arrowsBackBlue.png" )}/>    
                }else{
                    var tempNotificationIconName = <Image style={styleCss.detailsBoltOnsImage} source={require( "../images/arrowsBackBlack.png")}/>                        
                }
                break;
            case 'accepted':
                if(item.read == 0){
                    var tempNotificationIconName = <Image style={styleCss.detailsBoltOnsImage} source={require(  "../images/checkGreen.png" )}/>        
                }else{
                    var tempNotificationIconName = <Image style={styleCss.detailsBoltOnsImage} source={require(  "../images/checkBlack.png")}/>                        
                }
                break;
            case 'rejected':
                if(item.read == 0){
                    var tempNotificationIconName = <Image style={styleCss.detailsBoltOnsImage} source={require("../images/xRed.png" )}/>        
                }else{
                    var tempNotificationIconName = <Image style={styleCss.detailsBoltOnsImage} source={require(  "../images/xBlack.png")}/>                        
                }
                break;             
            default:
                var tempNotificationIconName = null
                break;
        }
        return(
            <View style={[styleCss.detailsIconTextContainer,{paddingTop:30}]}>
                <View style={styleCss.detailsContentIconContainer}>
                    {tempNotificationIconName}
                </View>
                <View >
                    <Text style={styleCss.notificationTitle}>
                        {moment(item.create_time).format("MMM[.] D")} | {item.title}
                    </Text>
                    <Text style={styleCss.notificationSubtitle}>
                        {item.text}
                    </Text>
                </View>
            </View>
        )
    }
    async deleteNotification(notification){
        this.setState({
            isRefreshing: true
        })
        try{
            await api.deleteNotification(notification.item.id).then((res)=>{
                if(res.status){
                    api.getNotifications().then((_res)=>{
                        if(_res.status){
                            this.setState({
                                notificationsList : _res.data.list
                            })
                        }else{
                            alert('This action failed!, Please check your internet connection!')                            
                        }
                    })
                }else{
                    alert('This action failed!, Please check your internet connection!')
                }
            })
        }catch(e){
            alert('This action failed!, Please check your internet connection!')
            console.log(e)
        }finally{
            this.setState({
                isRefreshing: false
            })
        }
    }
    async readNotification(notification){
        this.setState({
            isRefreshing: true
        })
        try{
            await api.readNotification(notification.item.id).then((res)=>{
                if(res.status){
                    api.getNotifications().then((_res)=>{
                        if(_res.status){
                            this.setState({
                                notificationsList : _res.data.list
                            })
                        }
                    })
                }
            })
        }catch(e){
            alert('This action failed!, Please check your internet connection!')
            console.log(e)
        }finally{
            this.setState({
                isRefreshing: false
            })
        }
    }
    showNotifications(){
        try{
            if(this.state.notificationsList.length>0){
                return(
                    <SwipeListView   
                        ref={'swipeListView'} 
                        useFlatList
                        closeOnRowBeginSwipe={true}
                        disableRightSwipe={true}
                        closeOnScroll={false}
                        closeOnRowPress
                        style={{backgroundColor:"#FFF"}}                
                        renderItem={({item}) => (this.renderNotifications(item))}                                       
                        data={(this.state.notificationsList)}                            
                        renderHiddenItem={ (data, rowMap, secId, rowId) => (
                            <TouchableHighlight style={styleCss.rowBack}>
                                <View style={styleCss.rowBack}>
                                    <Text></Text>
                                    <View style={{flexDirection:"row",height:60}}>
                                        <TouchableHighlight 
                                            onPress={()=> (this.refs.swipeListView.safeCloseOpenRow(),this.readNotification(data))}
                                            style={[styleCss.notificationButton,{backgroundColor:"#FA9917",}]}>
                                            <Image style={{height:30,width:30,}} source={require("../images/bellWhite.png")}/>    
                                        </TouchableHighlight>
                                        <TouchableHighlight 
                                            onPress={()=>( this.deleteNotification(data),this.readNotification(data))}
                                            style={[styleCss.notificationButton,{backgroundColor:"#FF5F58",}]}>
                                            <Image style={{height:30,width:30,}} source={require("../images/trashWhite.png")}/>    
                                        </TouchableHighlight>
                                    </View>
                                </View>
                            </TouchableHighlight>
                        )}
                        leftOpenValue={0}
                        rightOpenValue={-120}
                        onEndReachedThreshold = {-10}
                        keyExtractor={(item)=>item.id}   
                    />   
                )
            }else{
                return (
                    <View style={styleCss.detailsIconTextContainer}>
                        <View style={styleCss.detailsContentIconContainer}>
                            <Icon type={'ionicon'} name={'ios-alert-outline'} size={18} color={ "#5C6979"}/>                                                           
                        </View>
                        <View >
                            <Text allowFontScaling={false} style={styleCss.detailsIconText}>
                                No Notifications
                            </Text>
                        </View>
                    </View>
                )
            }
        }catch(e){
            console.log(e)
        }
    }  
    getReadableDate(date){
        if(date!=="" && date!== null){
            try {
                var endDate = (moment(date).format('MMMM-D-YYYY')).toUpperCase()
                return(endDate)
            }catch(error){
                console.log("Cant get starting date " + error);   
                return("N/A")         
            }
        }
    }
    async componentWillMount() {       
        this.getAccount()
    }
    render(){
        return(
            <View  style={styleCss.mainViewContainer}>
                <View style={[styleCss.sendNDAHeader,{paddingTop:29, paddingBottom:10}]}>
                    <View style={ styleCss.headerTextContainer}>
                        <Text style={styleCss.headerText}>Account</Text>
                    </View>
                    <View style={styleCss.IconAccount}>
                        <Icon type={'ionicon'}  name={'ios-log-out'} style={styleCss.IconAccount} onPress = { async () => restart()}/>    
                    </View>                 
                </View>
                <ScrollView 
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.isRefreshing}
                            onRefresh={()=>this.getAccount()}
                            tintColor="#3498db"
                            title="Loading..."
                            colors={['#3498db', '#6699ff', '#3377ff']}
                            progressBackgroundColor="#ccddff"/>
                    }
                >
                <View style={[styleCss.avatarContainer]}>
                    <Avatar
                        medium
                        rounded
                        source={this.state.user.picture ? {uri: this.state.user.picture}:require('../images/_blank.jpg')}
                        activeOpacity={0.7}
                    />
                    <View style={{marginTop:16}}>
                        <Text>{this.state.user.first_name} {this.state.user.last_name}</Text>
                    </View>
                    <View style={{marginBottom:16}}>
                        <Text style={styleCss.italicText}>{this.state.companies.name?this.state.companies.name:""}</Text>
                    </View>
                </View>
                <View style={styleCss.planContainer}>
                    <Text style={styleCss.remainingNdasText}>{this.state.limitInfo.invites_used_this_month}/ {(this.state.limitInfo.normal_limit<1?"UNLIMITED":this.state.limitInfo.normal_limit)}</Text>
                    <Text style={styleCss.cyclesNdasText}>Next Cycle: {this.getReadableDate(this.state.endMonth)}</Text>
                </View>
                <View style={{paddingTop:14}}>
                    <Text style={styleCss.detailsSubTitle}>
                        RECENT NOTIFICATIONS
                    </Text>
                </View>
                {/*HARDCODE*/}
                {this.showNotifications()}
                <View>
                    <View style={{paddingBottom:20}}/>                        
                    </View>               
                </ScrollView>   
            </View>         
            );
        }
    }

