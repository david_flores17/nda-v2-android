import React, {Component} from 'react'; 
import  {
    View,
    ScrollView,
    Text,
    StyleSheet,
    Dimensions,
    FlatList,
    TouchableWithoutFeedback,
    Image,
    TouchableOpacity
        } from 'react-native';
import { NavigationActions } from 'react-navigation'
import { RadioButtons } from 'react-native-radio-buttons'
import { Icon } from 'react-native-elements';
import styleCss from '../config/css';
import Globals from './Globals.js'
import api from '../config/api';
const _window = Dimensions.get('window');
function renderNdaTypes(option, selected, onSelect, index){
    const style = selected ? { fontWeight: 'bold'} : {};
    return (
        <TouchableWithoutFeedback onPress={onSelect} key={index}>
            <View style={styleCss.radioButtonsContainer}>  
                <Text style={style}>{option.name}</Text>
                {!selected ? <Icon type={'entypo'} name={'circle'} size={18} color={ "#e8e8e8"}/> : <Icon type={'entypo'} name={'vinyl'} size={18} color={ "#2296f2"}/>}                                                           
            </View>
        </TouchableWithoutFeedback>
    );
}
function renderNdaPurpuse(option, selected, onSelect, index){
    const style = selected ? { fontWeight: 'bold'} : {};
    return (
        <TouchableWithoutFeedback onPress={onSelect} key={index}>
            <View style={styleCss.radioButtonsContainer}>  
                <View style={{width: _window.width -100}}>
                    <Text style={style}>{option.name}</Text>
                    <Text style={styleCss.radioButtonSubtitle}>{Globals.toCammelCase(option.purpose)}</Text>
                </View>
                {!selected ? <Icon type={'entypo'} name={'circle'} size={18} color={ "#e8e8e8"}/> : <Icon type={'entypo'} name={'vinyl'} size={18} color={ "#2296f2"}/>}                                                           
            </View>
        </TouchableWithoutFeedback>
    );
}

function renderContainer(optionNodes){
    return <View style={{marginBottom:16}}>{optionNodes}</View>;
}
export default class SendNDA1 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ndaTypeSelected:null,
            ndaPurposeSelected:null,
            ndaTypes:[],
            ndaPurpose:[],
            loaded:false,
            ndaParametersToSend:{}
        }
    }
    onValidation(){
        if(this.state.ndaPurposeSelected && this.state.ndaTypeSelected){
            this.state.ndaParametersToSend.type = this.state.ndaTypeSelected.id
            this.state.ndaParametersToSend.purpose = this.state.ndaPurposeSelected.id
            this.props.navigation.navigate('SendNDA2',this.state.ndaParametersToSend)
        }else{

        }
    }
    ifReadyToNextScreen(){
        if(this.state.ndaPurposeSelected && this.state.ndaTypeSelected){
            return(
                <Icon type={'ionicon'} name={"ios-arrow-round-forward-outline"} size={35} color={"#000"} onPress={()=>this.onValidation()}/>                                 
            )            
        }else{
            return(
                <Icon type={'ionicon'} name={"ios-arrow-round-forward-outline"} size={35} color={"#BDBDBD"} />                                 
            )
        }
    }
    componentWillMount(){
        try{
            api.getNdaParameters().then((res)=>{
                if(res.status){
                    this.setState({
                        ndaTypes:res.data.agreement_types,
                        ndaPurpose:res.data.agreement_purposes,
                    })
                    Globals.ndaParameters = res.data
                }else{
                    alert("Service is temporarily not available, please try again later")
                }
            })
            api.getNoticeParty().then((res)=>{
                console.log("notice parties",res.data)
                if(res.status){
                    Globals.noticeParties = res.data
                }else{
                    alert("Service is temporarily not available, please try again later")
                }
            })
        }catch(e){
            console.log(e)
        }
        finally{
            this.setState({loaded:true})
        }
    }
    render(){
        return(
            <View style={[styleCss.mainViewContainer,{backgroundColor:"#e8e8e8"}]}>
                 <View style={[styleCss.sendNDAHeader,{paddingTop:29, paddingBottom:10}]}>
                    <View style={ styleCss.headerTextContainer}>
                        <Text style={styleCss.headerText}>Step 1 of 3</Text>
                    </View>
                    <View style={[{height:44,paddingBottom:20}]}>
                    {this.ifReadyToNextScreen()}
                    </View >
                </View>
                <ScrollView >
                <View style={styleCss.sendNDA1TitleContainer}>
                    <Text>NDA TYPE:</Text>
                </View>
                <RadioButtons
                    options={ this.state.ndaTypes }
                    onSelection={(selectedOption)=> this.setState({ndaTypeSelected:selectedOption})}
                    selectedOption={this.state.ndaTypeSelected }
                    renderOption={ renderNdaTypes }
                    renderContainer={ renderContainer }
                />  
                <View style={styleCss.sendNDA1TitleContainer}>
                    <Text>PURPOSE:</Text>
                </View>
                <RadioButtons
                    options={ this.state.ndaPurpose }
                    onSelection={ (selectedOption)=>this.setState({ndaPurposeSelected:selectedOption}) }
                    selectedOption={this.state.ndaPurposeSelected }
                    renderOption={ renderNdaPurpuse }
                    renderContainer={ renderContainer }
                />  
                </ScrollView>
            </View>
        )    
    }
}
