import React, { Component } from 'react'
import {
    StyleSheet,
    View,
    ScrollView,
    TextInput,
    Dimensions,
    Text,
    TouchableOpacity,
    AsyncStorage,
    Alert,
} from 'react-native';
import DropdownAlert from 'react-native-dropdownalert';
import {BallIndicator} from 'react-native-indicators';
import {Icon} from 'react-native-elements';
const _window = Dimensions.get('window');      
import styleCss from '../config/css.js'
import api from '../config/api.js'
import Globals from './Globals.js'
export default class AddNoticeParty extends Component {
    constructor(props) {
        super(props);
        this.state = {  
            email : "",
            fetching : false
        };
    }
    validateEmail(emailField){
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/
        if (reg.test(emailField) == false)
        {
            return false;
        }
        return true;
    }
    async AddNoticeParty(){
        try{
            if( (this.validateEmail(this.state.email)) && (!this.state.fetching)){
                this.setState({fetching : true})
                api.createNoticeParty(this.state.email).then((res)=>{       
                    console.log("add notice party res",res)
                    if(res.status){
                        Globals.noticeParties.push({
                            id : Globals.noticeParties.length > 0?(parseInt(Globals.noticeParties[Globals.noticeParties.length-1].id) + 1):"0",
                            email : this.state.email
                        })      
                        Globals.updateNdaNoticeParties = true
                        Alert.alert(
                            'Email added',
                            'The email '+this.state.email+' was added successfully as notice party email.',[
                            {text: 'OK', onPress: async () => ( this.props.navigation.goBack() )},
                            {text: 'ADD ANOTHER', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                        ]);  
                    }else{
                        Alert.alert(
                            'NDA found an error',
                            res.error.message,[
                            {text: 'OK', onPress: async () => ( this.props.navigation.goBack() )},
                            {text: 'TRY AGAIN', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                        ]);  
                    }
                })
            }else{
                alert("Ivalid notice party email address.")
            }        
        }catch(e){
            alert("Something went wrong, please try again later.")            
        }
        finally{
            this.setState({ fetching : false })
        }
    }
    ifCheckIconReady(){
        if(this.validateEmail(this.state.email)){
            if(this.state.fetching){
                return(
                    <View style={{left : -8}}>
                        <BallIndicator color="#2296f2" size={20}/>       
                    </View>
                )
            }else{
                return(
                    <Icon type={'ionicon'} name={"ios-checkmark"} size={40} color={"#000"} onPress={() => (this.AddNoticeParty())}/>                     
                )            
            }
        }else{
            return(
                <Icon type={'ionicon'} name={"ios-checkmark"} size={40} color={"#bdbdbd"} onPress={() => this.AddNoticeParty()}/>                     
            )
        }
    }
    showHint(type){
        if(type == "email"){
            this.dropdown.alertWithType('custom', 'Email:', "Email from your notice party.");
        }else if(type == "inviteeEmail"){
            this.dropdown.alertWithType('custom', 'Invitee Email:', "Who is receiving this NDA.");            
        }
    }
    render() {
        return (
            <View style={[styleCss.mainViewContainer]}>
             <View style={styleCss.sendNDAHeader}>
                    <View style={styleCss.iconHeaderContainer}>
                        <Icon type={'ionicon'} name={"ios-arrow-round-back-outline"} size={35} color={"#000"} onPress={()=>this.props.navigation.goBack()}/>                     
                    </View>
                    <View style={ styleCss.titleHeaderContainer}>
                        <Text  style={[styleCss.headerText]}>Add New Notice Party</Text>
                    </View>
                    <View style={styleCss.iconHeaderContainer}>
                    {this.ifCheckIconReady()}
                    </View>
                    </View>   
                <ScrollView>    
                    <View style={[styleCss.sendNDA2TitleContainer,styleCss.row]}>
                        <Text onPress={()=>Keyboard.dismiss()}>Email:</Text>
                        <Icon type={'simple-line-icon'} name={'question'} size={18} color={"#000"} onPress={()=>{this.showHint('email')}}/>                                         
                    </View>
                    <View style={styleCss.inputContainer}>
                        <TextInput
                            clearButtonMode='while-editing'
                            style={styleCss.formInput}
                            editable = {true}
                            placeholder="myname@mydomain.com"
                            keyboardType="email-address"
                            returnKeyType="next"
                            autoCapitalize="none"
                            autoCorrect={false}
                            onChangeText={(text) => this.setState({email:text})}
                            onSubmitEditing={()=>this.emailInput.focus()}
                            ref={(input)=>this.ndaNameInput=input}
                        />
                    </View>
                </ScrollView>
                    <DropdownAlert 
                        ref={ref => this.dropdown = ref} 
                        imageSrc={require("../images/information.png")}
                        containerStyle={styleCss.customAlertContainerNdaName}
                        closeInterval={8000}
                    />
            </View>
        );
    }
}