import React, { Component } from 'react';
import { 
  View,
  Text,
  ScrollView,
  StyleSheet,
  Dimensions,
  ActivityIndicator,
  ListView,
  AsyncStorage,
  Actions,
  TouchableHighlight,
  Alert,
  FlatList,
  Image,
} from 'react-native';
import DropdownAlert from 'react-native-dropdownalert';
import {BallIndicator} from 'react-native-indicators';
import { NavigationActions } from 'react-navigation'
import api from '../config/api';
import Dashboard from './Dashboard';
import Globals from './Globals.js';
import styleCss from '../config/css.js';
import {Icon} from 'react-native-elements';
import moment from 'moment'
const ACCESS_TOKEN = 'access_token';
const USER_ID = 'user_id';
const _window = Dimensions.get('window');
const resetAction = NavigationActions.reset({
  index: 0,
  actions: [
    NavigationActions.navigate({ routeName: 'Dashboard'})
  ]
})
class NdaDetail extends Component {
  constructor(props){
    super(props);
    this.state = {
      fullNDA:[],   
      loaded:false,
      user_Id:0,
      ndaStatus:'Loading'  ,
      startDate :new Date(),
      sendingEmail:false,
      emailSended:false,
      errorSendingEmail:false
    }     
  }
  navigate(){
    this.props.navigation.dispatch(resetAction)   
  }
  async apiDeleteNda(){
    this.setState({loaded:false})
    try{
        await api.deleteNDA(this.state.fullNDA)
        .then((res)=>{
            if(res.status){
                Globals.ndaOffset = 0        
                Globals.updateNdaDashboard = true
                this.props.navigation.dispatch(resetAction)
            }else{
                alert(res.error.description)
            }
        })  
    }catch(e){
      console.log(e)
      alert('This action failed!, Please check your internet connection!')      
    }finally{
      this.setState({loaded:true})      
    }   
  }
  acceptNda(){
    this.setState({loaded:false})
    try {
      api.acceptNDA(this.state.fullNDA).then((res)=>{
        if(res.status){
          Globals.ndaOffset = 0
          Alert.alert(res.data.name,"Was "+res.data.status_type.name+ " Successfully"
          // ,{text: 'OK', onPress: () => this.props.navigation.dispatch(resetAction)}
        )
          Globals.updateNdaDashboard = true
          this.setState({
            ndaStatus:'Accepted',
            loaded:true,
          })  
        }else{
          alert('This action failed!, Please check your internet connection!')
        }
      })    
    }catch(error){
      alert('This action failed!, Please check your internet connection!')      
      console.log(" Somenthing went wrong: "+error)
    }     
  }
  refuseNda(){
    this.setState({loaded:false})
    try {
      api.refuseNDA(this.state.fullNDA).then((res)=>{
        if(res.status){
          Globals.ndaOffset = 0          
          Alert.alert(res.data.name,"Was "+res.data.status_type.name+ " Successfully"
          // ,{text: 'OK', onPress: () => this.props.navigation.dispatch(resetAction)}
        ) 
          Globals.updateNdaDashboard = true
          this.setState({
            ndaStatus:'Refused',
            loaded:true,
          })  
        }else{
          alert('This action failed!, Please check your internet connection!')          
        }
      })    
    }catch(error){
      console.log(" Somenthing went wrong: "+error)
    }
  }
  deleteNdaMessage(){
    try {
        Alert.alert(
            'Delete NDA?',
            "Are you sure you want to delete this NDA permanently?",[
            {text: 'OK', onPress: async () => this.apiDeleteNda(this.state.fullNDA)},
            {text: 'Cancel', onPress: () => null, style: 'cancel'},
        ]);                   
    } catch(error) {
        console.log("Something went wrong, token not deleted " + error);
    }  
  }
  customHeader(){
    if(this.state.ndaStatus=='Refused'){
      return(
        <View style={styleCss.detailsTitleRefused}>  
          <Text style={styleCss.detailsStatusTitle}>
            {"REJECTED"}
          </Text>
          <Text style={styleCss.detailsStatusDate}>
            {this.getReadableDate(this.state.fullNDA.status_date)}
          </Text>
        </View>
      )
    }
    if(this.state.ndaStatus=='Pending'){
      return(
        <View style={styleCss.detailsTitlePending}>  
          <Text style={styleCss.detailsStatusTitle}>
            {"PENDING"}
          </Text>
          {this.ifResendInvite()}
        </View>
      )
    }
    if(this.state.ndaStatus=='Accepted'){
      return(
        <View style={styleCss.detailsTitleAccepted}>  
          <Text style={styleCss.detailsStatusTitle}>
            {"ACCEPTED"}
          </Text>
          <Text style={styleCss.detailsStatusDate}>
            {this.getReadableDate(this.state.fullNDA.status_date)}
          </Text>
        </View>
      )
    }
  }
  getReadableDate(_Date){
    try{
      tempResult = moment(_Date).format("MMMM DD, YYYY")
      return(tempResult)
    }catch(e){
      console.log(e)
    }
  }
  renderLoaded(){
    try{

      return(
        <View style={[styleCss.mainViewContainer,{backgroundColor:"#e8e8e8"}]}>
          <View style={styleCss.sendNDAHeader}>
              <View style={styleCss.iconHeaderContainer}>
                <Icon type={'ionicon'} name={"ios-arrow-round-back-outline"} size={35} color={"#000"} onPress={()=>this.props.navigation.goBack()}/>                     
              </View>
              <View style={styleCss.titleHeaderContainer}>
                  <Text  style={[styleCss.headerText,]}>NDA Details</Text>
              </View>
              <View style={styleCss.iconHeaderContainer}>                 
                <Icon type={'evilicon'} name={"eye"} size={35} color={"#000"} onPress={()=>this.props.navigation.navigate('HtmlView', this.state.fullNDA)}/>                     
              </View>
          </View>
          <ScrollView >
            <View style={styleCss.scrollViewContainerBackgroundColor}>
              <Text style={styleCss.detailsTitle}>{this.state.fullNDA.name}</Text>
              {this.customHeader()} 
              <View style={styleCss.detailsContentContainer}>
                <Text style={styleCss.detailsSubTitle}>START DATE: {this.getReadableDate(this.state.fullNDA.start_date)}</Text>
                <Text style={styleCss.detailsBlurText}>Type: {this.state.fullNDA.agreement_type.name}</Text>
              </View>
              <View style={styleCss.detailsContentContainer}>
                <Text style={styleCss.detailsSubTitle}>NDA DETAILS:</Text>
                <Text style={styleCss.detailsText}>Jurisdiction: {this.state.fullNDA.jurisdiction.name}</Text>
                <Text style={styleCss.detailsText}>Term: {this.state.fullNDA.term.term}</Text>
              </View>
              <View style={styleCss.detailsContentContainer}>
                <Text style={styleCss.detailsSubTitle}>INVITOR </Text>
                <View style={styleCss.detailsIconTextContainer}>
                  <View style={styleCss.detailsContentIconContainer}>
                    <Icon type={'simple-line-icon'} name={'user'} size={16} color={ "#5C6979"}/>                                                           
                  </View>
                  <View >
                    <Text allowFontScaling={false} style={styleCss.detailsIconText}>
                      {this.state.fullNDA.invitor.first_name+" "+this.state.fullNDA.invitor.last_name}
                    </Text>
                  </View>
                </View>
              </View>
              <View style={styleCss.detailsContentContainer}>
                <Text style={styleCss.detailsSubTitle}>INVITEE</Text>
                <View style={styleCss.detailsIconTextContainer}>
                  <View style={styleCss.detailsContentIconContainer}>
                    <Icon type={'simple-line-icon'} name={'user'} size={16} color={ "#5C6979"}/>                                                           
                  </View>
                  <View >
                    <Text allowFontScaling={false} style={styleCss.detailsIconText}>
                      {( this.state.fullNDA.invitee.first_name ? this.state.fullNDA.invitee.first_name+" "+this.state.fullNDA.invitee.last_name : this.state.fullNDA.invitee.email)}
                    </Text>
                  </View>
                </View>
              </View>
              <View style={styleCss.detailsContentContainer}>
                <Text style={styleCss.detailsSubTitle}>BOLT ONS</Text>
                  {this.getBoltOns()}
              </View>
              <View style={styleCss.detailsContentContainer}>
                <Text style={styleCss.detailsSubTitle}>THIRD PARTIES</Text>
                  {this.getThirdParties()}
              </View>
              <View>
              {this.deteleNdaText()}
              </View>
              <DropdownAlert 
                ref={ref => this.dropdown = ref} 
                imageSrc={require("../images/envelopeWhiteHD.png")}
                //onClose={data => this.onClose(data)} 
                containerStyle={styleCss.customAlertContainer}
              />
              <View style={{marginBottom:30}}/>
            </View>
          </ScrollView>
            {this.ifNdaInvitee()}
        </View>
      )        
    } catch(e){console.log(e)}
  }
  deteleNdaText(){
    console.log(this.state.fullNDA.invitee_id, this.state.user_Id, this.state.ndaStatus)
    try{
      if((this.state.fullNDA.invitee_id !== this.state.user_Id) && (this.state.ndaStatus == 'Pending')){
        return(
          <View>
            <TouchableHighlight onPress={()=>{ this.deleteNdaMessage()}}>  
              <Text style={styleCss.WarningText}>DELETE NDA</Text>
            </TouchableHighlight>   
           </View>
          
        )
      }else{
        return null;
      }
     } catch(e){
      console.log(e)
    }
  }
  renderThirdParties(item){
    return (
      <View style={styleCss.detailsIconTextContainer}>
        <View style={styleCss.detailsContentIconContainer}>
          <Icon type={'simple-line-icon'} name={'people'} size={20} color={ "#5C6979"}/>                                                                     
        </View>
        <View style={styleCss.centering}>
          <Text allowFontScaling={false} style={styleCss.detailsIconText}>
            {item.email}
          </Text>
        </View>
      </View>
    )
  }
  renderBoltOns(item){
    switch(item.id) {
        case '14':
          var tempBoltOnsIconName = <Image style={styleCss.detailsBoltOnsImage} source={require("../images/hand-paperHD.png")}/>    
          break;
        case '15':
          var tempBoltOnsIconName = <Image style={styleCss.detailsBoltOnsImage} source={require("../images/paperclipHD.png")}/>        
          break;
        case '16':
          var tempBoltOnsIconName = <Image style={styleCss.detailsBoltOnsImage} source={require("../images/repeatHD.png")}/>        
          break;
        case '18':
          var tempBoltOnsIconName = <Image style={styleCss.detailsBoltOnsImage} source={require("../images/notes-medicalHD.png")}/>        
          break;
        case '19':
          var tempBoltOnsIconName = <Image style={styleCss.detailsBoltOnsImage} source={require("../images/stopwatchHD.png")}/>        
          break;
        case '26':
          var tempBoltOnsIconName = <Image style={styleCss.detailsBoltOnsImage} source={require("../images/handshakeHD.png")}/>        
          break;
        case '29':
          var tempBoltOnsIconName = <Image style={styleCss.detailsBoltOnsImage} source={require("../images/universityHD.png")}/>        
          break;
        case '31':
          var tempBoltOnsIconName = <Image style={styleCss.detailsBoltOnsImage} source={require("../images/banHD.png")}/>            
          break;
        default:
          var tempBoltOnsIconName = <Image style={styleCss.detailsBoltOnsImage} source={require("../images/banHD.png")}/>                    
          break;
    }
    return(
      <View style={styleCss.detailsIconTextContainer}>
        <View style={styleCss.detailsContentIconContainer}>
          {tempBoltOnsIconName}
        </View>
        <View style={styleCss.centering}>
          <Text allowFontScaling={false} style={styleCss.detailsIconText}>
            {item.name}
          </Text>
        </View>
      </View>
    )
  }
  getThirdParties(){
    try{
      if(this.state.fullNDA.third_parties.length > 0 ){
        return(
          <FlatList
            data={( this.state.fullNDA.third_parties)}
            renderItem={({item}) => (this.renderThirdParties(item))} 
            keyExtractor={(item)=>item.id}   
          />  
        )
      }else{
        return (
          <View style={styleCss.detailsIconTextContainer}>
            <View style={styleCss.detailsContentIconContainer}>
              <Icon type={'ionicon'} name={'ios-alert-outline'} size={18} color={ "#5C6979"}/>                                                           
            </View>
            <View >
              <Text allowFontScaling={false} style={styleCss.detailsIconText}>
                No Third Parties
              </Text>
            </View>
          </View>
        )
      }
    }catch(e){
      console/log(e)
    }
  }
  getBoltOns(){
    try{
      if(this.state.fullNDA.bolt_ons.length > 0 ){
        return(
          <FlatList
            data={( this.state.fullNDA.bolt_ons)}
            renderItem={({item}) => (this.renderBoltOns(item))} 
            keyExtractor={(item)=>item.id}   
          />  
        )
      }else{
        return (
          <View style={styleCss.detailsIconTextContainer}>
            <View style={styleCss.detailsContentIconContainer}>
              <Icon type={'ionicon'} name={'ios-alert-outline'} size={18} color={ "#5C6979"}/>                                                           
            </View>
            <View >
              <Text allowFontScaling={false} style={styleCss.detailsIconText}>
                No Bolt Ons
              </Text>
            </View>
          </View>
        )
      }
    }catch(e){
      console/log(e)
    }
  }
  renderLoadingView(){
    return(
      <View style={styleCss.loadingViewContainer}>
        <Text>
          Loading.....
        </Text>
        <ActivityIndicator 
            size="small"
            color="#2980b9"
            animating={this.state.fetching}/>
      </View>
    )
  }
  async getUserId(){
    try {
        let _user_Id = await AsyncStorage.getItem(USER_ID);
        this.setState({user_Id:_user_Id})
    }catch ( error) {
      console.log("something went wrong")
    }
  }
  async resendEmail(){
    try{
      this.setState({sendingEmail:true})
        setTimeout(()=>{
          api.resendEmail(this.state.fullNDA.id).then((res)=>{       
            if(res.status){
              this.dropdown.alertWithType('custom',"Success", 'Email has been sent');
            }else{
              this.dropdown.alertWithType('error', 'Error sending email', "There is a problem sending the email at this moment");
            }
          })
          this.setState({sendingEmail:false, emailSended:true})
        },1500)
    }catch(e){
      this.setState({sendingEmail:false, emailSended:false})
      this.dropdown.alertWithType('error', 'Error sending email', "There is a problem sending email at this moment");
      console.log(e)
    }
  }
  ifResendInvite(){
    if((this.state.fullNDA.invitor_id==this.state.user_Id)&&(this.state.ndaStatus=='Pending')){
      if(!this.state.sendingEmail){
        if(!this.state.emailSended){
          return(
            <View>
              <TouchableHighlight style={styleCss.resendBox} onPress={()=> this.resendEmail()}>  
                <Text style={styleCss.italicTextWhite}>Resend Invite</Text>
              </TouchableHighlight>
            </View>
          )
        }else{
          if(!this.state.errorSendingEmail){
            return(
              <View style={{width:90}} >  
                  <Icon type={'simple-line-icon'} name={'check'} size={20} color={ "#FFF"}/>                                                           
              </View>
            )
          }else{
            return(
              <View style={{width:90}} >  
                  <Icon type={'feather'} name={'alert-triangle'} size={25} color={ "#FFF"}/>                                                           
              </View>
            )
          }
        }
      }else{
        return(
          <View style={styleCss.resendIndicatorContainer}>  
              <BallIndicator color="#FFF" size={20}/>       
          </View>        
        )
      }
    }else{
      return null;
    }
  }
  ifNdaInvitee(){
    try{
      if ((this.state.fullNDA.invitee_id==this.state.user_Id)&&(this.state.ndaStatus=='Pending')) {
        return(
          <View>
            <View style={styleCss.separator}/>          
            <View style={styleCss.detailsButtonsContainer}>
              <TouchableHighlight 
                  style={[styleCss.detailsButton,{backgroundColor:'#2ac940'}]}
                  onPress={() => this.acceptNda()}>
                <Text style={styleCss.buttonText}>ACCEPT</Text>
              </TouchableHighlight>  
              <TouchableHighlight style={{width:_window.width/100}}><Text> </Text></TouchableHighlight>
              <TouchableHighlight 
                  style={[styleCss.detailsButton,{backgroundColor:'#FF5f58'}]}
                  onPress={() => this.refuseNda()}>
                <Text style={styleCss.buttonText}>REJECT</Text>
              </TouchableHighlight>     
            </View>
          </View>
        )
      }else{
        return null;
      }
    }catch ( error) {
      console.log("something went wrong ",error)
    }
  }

  componentWillMount() {
    this.getUserId()
    api.ndaDetails(this.props.navigation.state.params).then((res)=>{  
      console.log(res)     
      if(res.status){
        this.setState({
          fullNDA: res.data,
          loaded:true,
          ndaStatus:res.data.status_type.name,
          startDate: res.data.start_date
        })
      }else{
        alert('This action failed!, Please check your internet connection!')
      }
    })  
  }    
  render() {
    if(!this.state.loaded){
      return this.renderLoadingView();
    }else{           
      return this.renderLoaded();
    }  
  }
}
export default NdaDetail;