import React from 'react';
import {
    Image,
    StyleSheet,
    Navigator,
    View,
    TouchableOpacity,
    Button,
    Linking,
    Alert,
    AsyncStorage,
    Platform
    } from 'react-native'; 
import {
    TabNavigator,
    StackNavigator,    
} from 'react-navigation';
import {Icon} from 'react-native-elements';
import SendNDA1 from './SendNDA1';
import Dashboard from './Dashboard';
import Account from './Account';
import NdaDetail from './NdaDetail' ;
import SendNDA2 from './SendNDA2';
import SendNDA3 from './SendNDA3';
import Webview from './Webview';
import HtmlView from './htmlView';
import LandingPage from './LandingPage';
import LoginForm from './LoginForm';
import AddNoticeParty from './AddNoticeParty';


export const sendNdaStack =  StackNavigator({
    SendNDA1:{
        screen:SendNDA1,   
        navigationOptions:{header: null}, 
    },
    SendNDA2:{
        screen:SendNDA2,
        navigationOptions: ({ navigation }) => ({header: null}),  
    },
    SendNDA3:{
        screen:SendNDA3,
        navigationOptions: ({ navigation }) => ({header: null}),  
    },
    AddNoticeParty:{
        screen:AddNoticeParty,
        navigationOptions: ({ navigation }) => ({header: null}),  
    },
});
export const DashboardStack = StackNavigator({
    Dashboard:{
        screen:Dashboard,
        navigationOptions:{header:null} 
    },
    NdaDetail:{
        screen:NdaDetail,
        navigationOptions: ({ navigation }) => ({header: null}),
     },  
    HtmlView:{
        screen: HtmlView,
        navigationOptions: ({ navigation }) => ({
            title: "Full NDA",
            headerStyle:{backgroundColor:'white'},   
            headerTitleStyle:{fontSize:16, fontWeight:"100"},      
            headerTintColor :'#000',
            headerLeft:
            <TouchableOpacity style={{width:50,}}onPress={()=> navigation.goBack()}>    
                <Icon type={'ionicon'}  name={'ios-arrow-round-back'} size={31} color={"#000"} onPress={()=> navigation.goBack()}/>                              
            </TouchableOpacity>
        }),
    }
});
export const LandingPageStack = StackNavigator({
    LandingPage:{
        screen:LandingPage,
        navigationOptions:{header:null} 
    },
    LoginForm:{
        screen:LoginForm,
        navigationOptions:{header:null} 
    },    

    Webview:{
        screen:Webview,
        navigationOptions: ({ navigation }) => ({
            title: "NDA NOW",
            headerStyle:{backgroundColor:'white'},   
            headerTitleStyle:{fontSize:16, fontWeight:"100"},      
            headerTintColor :'#000',
            headerLeft:
            <TouchableOpacity style={{width:50,}}onPress={()=> navigation.goBack()}>    
                <Icon type={'ionicon'}  name={'ios-arrow-round-back'} size={31} color={"#000"} onPress={()=> navigation.goBack()}/>                              
            </TouchableOpacity>
        }),
    }
});
export const AccountStack = StackNavigator({
   Account:{
        screen:Account,
        navigationOptions:{header:null}, 
     },
})
export const Tabs = TabNavigator({
    SendNDA:{
        screen:sendNdaStack,
        navigationOptions: ({ navigation }) => ({
            tabBarLabel:Platform.OS==='ios'?' ':'SEND NDA',
            tabBarIcon: ({tintColor,focused}) => <Image  source={focused ? require('../images/paperPlaneActive.png'): require('../images/paperPlaneInactive.png')} style={styles.icon}  color={tintColor}/>         
        }),  
    },
    Dashboard:{
        screen:DashboardStack,
        navigationOptions: ({ navigation }) => ({
            tabBarLabel:Platform.OS==='ios'?' ':'MANAGE',
            tabBarIcon: ({tintColor,focused}) => <Image  source={focused ? require('../images/inboxActive.png'): require('../images/inboxInactive.png')} style={styles.icon}  color={tintColor}/>         
        })
    },
    Account:{
        screen:AccountStack,
        navigationOptions: ({ navigation }) => ({
            tabBarLabel:Platform.OS==='ios'?' ':'ACCOUNT',
            tabBarIcon: ({tintColor,focused}) => <Image  source={focused ? require('../images/cogActive.png'): require('../images/cogInactive.png')} style={styles.icon}  color={tintColor}/>         
        }),  
    },
},
    TabNavigatorConfig ={
        initialRouteName :"Dashboard",
        swipeEnabled :false,
        tabBarOptions: {         
            lazy:true,
        },
    }
);
const styles = StyleSheet.create({
    icon: {
        width: 35,
        height: 35,
        bottom:-5
    },
});
//https://material.io/icons/ icon catalog