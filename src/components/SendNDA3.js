import React, {Component} from 'react';
import  {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    Switch,
    ScrollView,
    Alert,
    Dimensions,
    Image,

} from 'react-native';
import { NavigationActions } from 'react-navigation'
import api from '../config/api'; 
import styleCss from '../config/css';
import Globals from './Globals.js';
import { Icon,CheckBox } from 'react-native-elements';
import {BallIndicator} from 'react-native-indicators';
const _window = Dimensions.get('window');
const resetAction = NavigationActions.reset({
  index: 0,
  actions: [
   NavigationActions.navigate({ routeName: 'SendNDA1'}),
  ]
})

export default class SendNDA3 extends Component{
    constructor(props){
        super(props);
        console.log('Nda params 3/3',this.props.navigation.state.params)
        this.state = {
            loaded:false,
            ndaParamsToSend :this.props.navigation.state.params,
            boltOns: [],    
            selectedBoltOns:[],
            limitReached:false,
            showSuccessMessage:false,
            disableSend:true
        }     
    };
    onNext = () => {
        this.setState({disableSend:false})
        if(!this.state.limitReached){  
            var tempBoltOnsBeforeSend = []     
            this.state.boltOns.map((detail,index)=>(
                detail.checked ? tempBoltOnsBeforeSend.push({id:detail.id}) : null
            ))
            this.state.ndaParamsToSend.boltOns = tempBoltOnsBeforeSend
            console.log("params to api:",this.state.ndaParamsToSend);
            api.sendNDA(this.state.ndaParamsToSend)
                .then((res)=>{
                     if(res.status){
                            Globals.updateNdaAccount=true
                            Globals.updateNdaDashboard=true
                            //Alert.alert("Thanks for using NDA Now","Your NDA has been sent")    
                            this.setState({showSuccessMessage:true})
                            //this.props.navigation.dispatch(resetAction)
                            
                     }else{
                         Alert.alert("Oops, Something Went Wrong","It seems that something went wrong while sending your NDA. Please try again")
                     }
                     console.log(res)                    
                }) 

                setTimeout(()=>{
                    this.setState({
                     disableSend: false,
                   });
                 }, 5000)
        }else{
            alert("Thanks for using NDA NOW. You have sent your free NDA already. Please visit your profile page at www.myndanow.com to upgrade and keep sending NDAs.")
        }   
    }     
    renderLoadingView(){
        return(
            <View style={[styleCss.centering,{paddingTop:40,}]}>          
                <BallIndicator    color="#2296f2" size={20}/>
                <Text style={{marginTop:16}}>
                    Loading.....
                </Text>              
            </View>
        )
    }
    manageBolt(index,value){
        let boltOnsTemp = this.state.boltOns
        boltOnsTemp[index].checked ? boltOnsTemp[index].checked = false : boltOnsTemp[index].checked = true ;
        this.setState({
           boltOns: boltOnsTemp
        })
    }
    ifsuccessMessage(){
        if(this.state.showSuccessMessage){
            return(
                <View style={styleCss.successView}>
                    <Image
                        style={styleCss.successImage}
                        source={require('../images/checkGreen.png')}/>
                    <Text style={[styleCss.detailsStatusTitle,{marginBottom:8}]}>SUCESS!</Text>
                    <Text style={[styleCss.detailsStatusDate,{marginBottom:5}]}>Your new NDA has been sent.</Text>
                    <TouchableOpacity 
                        style={[styleCss.loginButton,{width:162}]}
                        onPress={()=>this.props.navigation.dispatch(resetAction)}
                        //onPress={this.onLoginPressed.bind(this)}
                        >
                        <Text style={styleCss.buttonText}>DONE</Text>
                    </TouchableOpacity>  
                </View>
            )
        }else{
            return null;
        }
    }
    ifdisableSend(){
        if(this.state.disableSend===true){
            return(
                <View style={[{height:44,paddingBottom:20}]}>
            <Icon type={'ionicon'} name={"ios-checkmark"} size={40} color={"#000"} onPress={() => this.onNext() }/> 
            </View>                 
            )
        }
        else{
            return(
            <View style={[{height:44,paddingBottom:20}]}>
            <Icon type={'ionicon'} name={"ios-checkmark"} size={40} color={"#FFF"}/> 
            </View>   
            )              
        }
    }
    renderLoaded(){
        return(
            <View style={[styleCss.mainViewContainer,{backgroundColor:"#e8e8e8"}]}>
                <View style={[styleCss.sendNDAHeader,{paddingTop:29, paddingBottom:10}]}>
                    <View style={[{height:44,paddingBottom:20}]}>
                        <Icon type={'ionicon'} name={"ios-arrow-round-back-outline"} size={35} color={"#000"} onPress={()=>this.props.navigation.goBack()}/>                     
                    </View>                   
                    <View style={ [styleCss.headerTextContainer,{marginRight:-1,marginLeft:-19}]}>
                        <Text  style={[styleCss.headerText]}>Step 3 of 3</Text>
                    </View>
                    {this.ifdisableSend()}        
                </View>
                <ScrollView >
                <View style={styleCss.sendNDA1TitleContainer}>
                    <Text>SELECT OPTIONAL BOLT-ONS</Text>
                </View>    
                    {this.state.boltOns.map((details,index) => ( 
                        <View style={styleCss.boltsOnsContainer} key={index}>    
                            <TouchableOpacity onPress={()=>{ this.manageBolt(index)}} style = {[styleCss.centering,{maxWidth:_window.width*.666}]}>
                                <Text onPress={()=>{ this.manageBolt(index)}} style = {styleCss.checkboxText}>{details.name}</Text>                        
                                {/*<Text style = {{fontSize:10}}>{details.description}</Text>>                        */}
                            </TouchableOpacity >
                            <View>
                                <CheckBox
                                    containerStyle={styleCss.checkboxContainer}
                                    iconRight={true}
                                    iconType="entypo"
                                    uncheckedIcon='circle'
                                    checkedIcon='vinyl'
                                    checkedColor="#2296f2"
                                    checked = {this.state.boltOns[index].checked}
                                    onPress={()=>{ this.manageBolt(index)}}
                                />
                            </View>
                        </View>
                        ))} 
                    <View style={{height:32}}/>
                </ScrollView>
                {this.ifsuccessMessage()}
            </View>
        )
    }
    async componentWillMount() {
        var tempBoltOns = []
        console.log(Globals.ndaParameters.bolt_ons)
        Globals.ndaParameters.bolt_ons.map((details,index)=>{
            tempBoltOns.push({
                index:tempBoltOns.length,
                id:details.id,
                name:details.name,
                description:details.description,
                checked: false,
            })
        })
        this.state.boltOns = tempBoltOns
        console.log("tempBoltOns",tempBoltOns)        
        await api.getUser().then((res)=>{
            if(res.status){
                this.setState({
                    limitReached:res.data.invite_limit_reached,
                    loaded:true
                })
            }else{
                alert('This action failed!, Please check your internet connection!')
            }
        })
    }  
    render(){
        if(!this.state.loaded){
            return this.renderLoadingView();
        }else{           
            return this.renderLoaded();
        }  
    }
}