import React, {Component} from 'react';
import  {
    View,
    StyleSheet,
    Image,
    TouchableOpacity,
    TextInput,
    ScrollView,
    ListView,
    ActivityIndicator,
    KeyboardAvoidingView,
    TouchableHighlight,
    Dimensions,
    Text,
    AsyncStorage,
    RefreshControl,
    AppState,
    Alert
} from 'react-native';
import RNRestart from 'react-native-restart';
import { SwipeListView } from 'react-native-swipe-list-view';
import { Container, Content} from 'native-base'
import {List, ListItem,SearchBar} from 'react-native-elements'
import {Platform} from 'react-native';
import FCM, {FCMEvent, RemoteNotificationResult, WillPresentNotificationResult, NotificationType} from 'react-native-fcm';
import api from '../config/api';
import Globals from './Globals.js'
const _window = Dimensions.get('window');
const USER_ID = 'user_id';
const ACCESS_TOKEN = 'access_token';
const DEVICE_TOKENADNROID = '@$device_token_android';
const isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
    const paddingToBottom = 20;
    return layoutMeasurement.height + contentOffset.y >=
      contentSize.height - paddingToBottom;
  };
// this shall be called regardless of app state: running, background or not running. Won't be called when app is killed by user in iOS
FCM.on(FCMEvent.Notification, async (notif) => {
    // there are two parts of notif. notif.notification contains the notification payload, notif.data contains data payload
    if(notif.local_notification){
      //this is a local notification
    }
    if(notif.opened_from_tray){
      //iOS: app is open/resumed because user clicked banner
      //Android: app is open/resumed because user clicked banner or tapped app icon
    }
    // await someAsyncCall();

    if(Platform.OS ==='ios'){
      //optional
      //iOS requires developers to call completionHandler to end notification process. If you do not call it your background remote notifications could be throttled, to read more about it see the above documentation link.
      //This library handles it for you automatically with default behavior (for remote notification, finish with NoData; for WillPresent, finish depend on "show_in_foreground"). However if you want to return different result, follow the following code to override
      //notif._notificationType is available for iOS platfrom
      switch(notif._notificationType){
        case NotificationType.Remote:
          notif.finish(RemoteNotificationResult.NewData) //other types available: RemoteNotificationResult.NewData, RemoteNotificationResult.ResultFailed
          break;
        case NotificationType.NotificationResponse:
          notif.finish();
          break;
        case NotificationType.WillPresent:
          notif.finish(WillPresentNotificationResult.All) //other types available: WillPresentNotificationResult.None
          break;
      }
    }
});
FCM.on(FCMEvent.RefreshToken, (token) => {
    console.log('token:',token)
    // fcm token may not be available on first load, catch it here
});
export default class Dashboard extends Component{
    constructor(props){
        super(props)
        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})
        this.state={
            isRefreshing:false  ,
            loaded:false,
            ndaList:[],
            sortedDeals:[],
            deletingNda:false,
            _color:'',
            dataSource: new ListView.DataSource({
                rowHasChanged: (r1, r2) => r1 !== r2,                    
            }),
            _notLoaded:false, 
            text:'',      
            bottomRefreshing : false
        };
        setInterval(() => {
            //console.log('44 Checking for updates should?:',Globals.updateNdaDashboard)
            if(Globals.updateNdaDashboard){
                console.log('45 UPDATING XXXXXXXXX')
                Globals.ndaOffset = 0
                this.getNdaList()
                Globals.updateNdaDashboard=false
                console.log('uploaded succefully')
            }else{
                //console.log('53 nothing to update')
            }
        }, 5000);
    }
       navigate(routeName){
        this.navigator.resetTo({
            name:routeName
        })
    }
    colorSet(status){
        if(status=='Pending'){
            return('#FF9500')
        }
         if(status=='Refused'){
            return('#D0011B')
        }
         if(status=='Accepted'){
           return('#5FB700')
        }
    }
    renderRow(rowData){
        //console.log('Row data name',rowData.name)
        return (     
            <TouchableHighlight>
                <View style={{backgroundColor:'white'}}>
                    <ListItem
                        title={rowData.name?rowData.name:'No title provided'}
                        badge={{value:rowData.status,backgroundColor:this.colorSet(rowData.status)}}
                        subtitle={rowData.invitee?rowData.invitee.email:"not created an account yet"}
                        onPress={() => this.onLearnMore(rowData)}
                        titleStyle={{color:'#383F49',height:20}}
                        subtitleStyle={{color:'#383F49',flexWrap: 'wrap',height:20}}
                    />
                </View>     
            </TouchableHighlight>
        )
    }
    ifDeleting(){
        if(this.state.deletingNda){
            return(
                <View style={styles.loadding}>          
                    <ActivityIndicator 
                        size="small"
                        color="#2980b9"
                        style={styles.spinner}
                        animating={this.state.fetching}/>            
                </View>
            )        
        }else{
            return null
        }
    }
    ifLoadingMoreNdas(){
        if(this.state.bottomRefreshing){
            return(
                <View style={{marginHorizontal : 8, alignItems : 'center', paddingBottom : 10}}>
                    <ActivityIndicator 
                        size="small"
                        color="#2980b9"
                    />
                    <Text>Loading...</Text>
                </View>
            )
        }else{
            return null;
        }
    }
    renderLoadingView(){
       return(
            <View style={styles.loadding}>          
                <ActivityIndicator 
                    size="large"
                    color="#2980b9"
                    style={styles.spinner}
                    animating={this.state.fetching}/>
                <Text>
                    Loading.....
                </Text>              
            </View>
       )
    }
    NotLoaded(){
        return(
            <ScrollView 
                refreshControl={
                    <RefreshControl
                        refreshing={this.state.isRefreshing}
                        onRefresh={this._onRefresh.bind(this)}
                        tintColor="#3498db"
                        title="Loading..."
                        colors={['#3498db', '#6699ff', '#3377ff']}
                        progressBackgroundColor="#ccddff"/>
                }
            >
                <View style={styles.pullbarr}>
                    <Text style={styles.title}>Pull down to refresh</Text>
                    <Text style={styles.title}>This list is empty!</Text>                   
                </View>
            </ScrollView>    
        )
    }
    async feedEndlessScroll(){
        if(!this.state.bottomRefreshing){
            //console.log('Fireing f()',this.state.ndaList);
            this.setState({bottomRefreshing : true})
            //this.setState({ loaded : false})
            //Globals.ndaOffset = Globals.ndaOffset + 10
            try{
                await api.getNDAS().then((res)=>{
                // console.log('Offset ',Globals.ndaOffset)
                // console.log('calling for more ndas ',res)
                Globals.ndaOffset = Globals.ndaOffset + res.data.total_length            
                //console.log( '+NDA list from api: ',res.data.list)
                //console.log('NDA list: ', this.state.ndaList)
                
                this.setState({
                     ndaList : [...this.state.ndaList , ...res.data.list],
                    // ndaList :  res.data.list,
                    // sortedDeals:res.data.list,
                    //bottomRefreshing:false,  
                })
                //console.log('Result :',this.state.ndaList)
                //sort method
                this.ds = this.state.dataSource.cloneWithRows(this.state.ndaList) 
                //this.setState({ dataSource: this.ds });
                // }).then( ()=>{
                //     this.setState({loaded:true,dataSource: this.ds})   
                // })  
                }).then((res)=>{ 
                    this.setState({
                        bottomRefreshing : false ,
                        dataSource: this.ds
                    })    
                })
            }catch(e){
                this.setState({bottomRefreshing : false})                    
                console.log(e)
            }
        }
    }
    renderLoaded(){
        return(
            <View>   
                <SearchBar        
                    clearButtonMode='while-editing'     
                    lightTheme
                    placeholderTextColor={'#383F49'}
                    style={styles.sBar}
                    onChangeText={(text) => this.filterSearch(text)}
                    placeholder='Search...' 
                    value={this.state.text}/>  
                {this.ifDeleting()}
                <ScrollView
                    onScroll={({nativeEvent}) => {
                        if (isCloseToBottom(nativeEvent)) {
                            this.feedEndlessScroll()
                        }
                    }}
                    scrollEventThrottle={400}                
                    style={{marginBottom:55}}
                    style={{marginBottom:55}}
                    ref={ref => this.ref_A = ref} parent={this}
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.isRefreshing}
                            onRefresh={this._onRefresh.bind(this)}
                            tintColor="#3498db"
                            title="Loading..."
                            colors={['#3498db', '#6699ff', '#3377ff']}
                            progressBackgroundColor="#ccddff" />
                    }>
                    <View >
                        <SwipeListView   
                            closeOnRowPress={true}
                            disableRightSwipe={true}
                            style={styles.listContainer}
                            renderRow={this.renderRow.bind(this)}
                            dataSource={this.state.dataSource}
                            renderHiddenRow={ (nda,secId, rowId, rowMap) => (
                                <TouchableHighlight
                                    style={styles.rowBack}
                                    onPress={( this.deleteNdaMessage.bind(this,nda,secId, rowId, rowMap))}
                                >
                                    <View  style={styles.rowBack}>
                                        <Text></Text>
                                        <Text style={{color:'white',fontWeight:'bold',paddingRight:15}}>Delete</Text>
                                    </View>
                                </TouchableHighlight>
                            )}
                            leftOpenValue={0}
                            rightOpenValue={-75}
                        />     
                    </View>       
                    {this.ifLoadingMoreNdas()}
                </ScrollView>   
            </View>
        )
    }
    deleteNdaMessage  = (nda,secId, rowId, rowMap) => {
        try {
            Alert.alert(
                'Are you sure you want to delete this NDA?',
                "you are about to delete permanently this NDA.",[
                {text: 'OK', onPress: async () => this.apiDeleteNda(nda,secId,rowId,rowMap)},//BackAndroid.exitApp(),
                {text: 'Cancel', onPress: () => rowMap[`${secId}${rowId}`].closeRow(), style: 'cancel'},
            ]);      
                    
        } catch(error) {
            console.log("Something went wrong, token not deleted " + error);
        }  
    }
    async apiDeleteNda(nda,secId, rowId, rowMap){
            console.log("nda to delete:",nda)
        if(nda.status=='Pending'){
            this.setState({deletingNda: true })        
            rowMap[`${secId}${rowId}`].closeRow()
            await api.deleteNDA(nda)
            .then((res)=>{
                console.log(res)
                if(res.status){
                    this.getNdaList()
                }else{
                    this.setState({deletingNda:false})
                    alert('The NDA you selected can not be removed because you are not the invitor for the NDA.')
                }
            })  
        }else{
            alert('You can not delete a NDA with '+nda.status+' status.')
        }    
    }
     filterSearch(text){
        const newData = this.state.ndaList.filter(function(item){
            const itemData = item.name.toUpperCase()
            const textData = text.toUpperCase()
            return itemData.indexOf(textData) > -1
        })
        this.setState({
            dataSource: this.state.dataSource.cloneWithRows(newData),
            text: text
        })
    }
    _sort(){
        //console.log(this.state.ndaList)
        this.state.sortedDeals.sort((a,b) => {
            if (a.create_time < b.create_time) {
                return 1;
            }
            if (a.create_time > b.create_time) {
                return -1;
            }
            return 0;
        })  
        //console.log("ndalist sorted")        
        //console.log(this.state.sortedDeals)        
        this.ds = this.state.dataSource.cloneWithRows(this.state.sortedDeals) 
        this.setState({
            dataSource: this.ds
        });
    }
    _onRefresh(){
        Globals.ndaOffset = 0
        this.setState({isRefreshing: true })
        this.feedList();
    }
    onLearnMore = (nda) => {
        console.log("detail data of " +nda.name)
        this.props.navigation.navigate('NdaDetail', nda);
    }

    async feedList(){
        await api.getNDAS()
        .then((res)=>{
            Globals.ndaOffset = Globals.ndaOffset + res.data.total_length
            console.log(res)
            this.setState({
                ndaList :  res.data.list,
                sortedDeals:res.data.list,
                loaded:true,     
            })
            this._sort()
        })  
        .then((res)=>{ 
                this.setState({
                    isRefreshing:false, 
                })    
        })
    }
    async deleteStoredData(){
        await AsyncStorage.removeItem(ACCESS_TOKEN)
        await AsyncStorage.removeItem(USER_ID)
        RNRestart.Restart()
    }

    getNdaList(){
       // console.log("accesing ndas[] with token:")
            api.getNDAS().then((res)=>{ 
            if(res.status){
                //console.log('Nda List:',res)
                Globals.ndaOffset = Globals.ndaOffset + res.data.total_length
                this.setState({ 
                    ndaList :  res.data.list,
                    sortedDeals:res.data.list,
                    loaded:true,
                    _notLoaded:false,
                    deletingNda:false
                })
                if(res.data.total_length==0){
                        this.setState({ _notLoaded:true })//empty List
                }
                this._sort()
            } 
            else{
                if(res.error.code=="err_token_expired"){
                    this.deleteStoredData()
                }
                this.setState({
                    _notLoaded:true
                })
                this.getNdaList()
            }  
        })           
    }
    async componentWillMount() {    
        //console.log('Loading dashboard')
        this.getNdaList()  //<------- remove '//'
        
    }
    async componentDidMount(){
        let token = await AsyncStorage.getItem(ACCESS_TOKEN);
        var tempGrantedPermissions=false
        FCM.requestPermissions().then( ()=>
            tempGrantedPermissions=true,
            //console.log('granted',tempGrantedPermissions)
        )
        .catch(()=>console.log('notification permission rejected'));
        await FCM.getFCMToken().then(deviceToken => {
            // store fcm token in your server
              AsyncStorage.getItem(DEVICE_TOKENADNROID, (err, result) => {
                if (result !== null) {
                    //console.log('Device token stored')
                    //console.log('API should have token stored',result)
                    //AsyncStorage.removeItem(DEVICE_TOKENANDROID)
                    //console.log('TOKEN Ereased')        
                }else{
                    //console.log('NO Device TOKEN STORED, connecting eipiai ')
                    try{
                       // console.log('request token:',token);
                        //console.log('device_token:',deviceToken)
                        api.registerDevice(token,'android',deviceToken).then((res)=>{
                            //console.log('API res:',res)          
                            if(res.status){
                                //console.log("Token was send successfully uploaded",res ) 
                                AsyncStorage.setItem(DEVICE_TOKENADNROID, deviceToken)
                                //console.log('local stored')
                            }else{
                                //console.log("Token was NOT STORED in api",res ) 
                                //console.log('2nd')                                
                                api.registerDevice(token,'android',deviceToken).then((res)=>{
                                //    console.log(res)
                                })
                            }
                        })
                    }catch(e){
                        console.log(e)
                    }  
                }  
            })
        });
    }
    render(){ 
        const navigation = this.props.navigation;     
        if(!this.state.loaded){
            if(this.state._notLoaded){
                return this.NotLoaded();
            }else{
                return this.renderLoadingView();
            }
        }else{   
            return this.renderLoaded();
        } 
    }
}
const styles = StyleSheet.create({
    listContainer:{
        backgroundColor:'white'   ,
    },
    container:{
       color:'#FFFFFF',
       justifyContent:'center',
    },
    sBar:{
        backgroundColor:'#F2F2F2',
        fontWeight:'bold',
        paddingLeft:50, 
        width:_window.width  ,
        height:50  ,
        color:'#1A284E'
    },
    title:{
        color:'#3498db',
        fontSize:15,
        textShadowOffset:{width:2,height:2},
        textShadowRadius:15,
        padding:20
    },
    pullbarr:{
        justifyContent:'center',
        alignItems:'center'
    },
    titleWrapper:{
        justifyContent:'center',
        flex:1
    },
     loadding:{
        padding:5,
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'white'
    },
    buttonContainer:{
        backgroundColor:'#3498db' ,
        opacity:0.8,
        padding:10,
        borderRadius:10,
        alignItems:'center'    
    },  
    buttonText:{
        color:'#FFFF' ,
        fontWeight:'900',
        fontSize: 17
    }, 
     rowsH:
    {
        flexDirection: 'row',
    }, 
	rowFront: {
		alignItems: 'center',
		backgroundColor: 'red',
		borderBottomColor: 'black',
		justifyContent: 'center',
        flex: 1,
	},
	rowBack: {
		alignItems: 'center',
		backgroundColor: '#e74c3c',
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'space-between',
		paddingLeft: 15,
	},
    backRightBtn: {
		alignItems: 'center',
		bottom: 0,
		justifyContent: 'center',
		position: 'absolute',
		top: 0,
		width: 75
	},
	backRightBtnLeft: {
		backgroundColor: 'blue',
		right: 75
	},
	backRightBtnRight: {
		backgroundColor: 'red',
		right: 0
	},
});

module.exports = Dashboard;
