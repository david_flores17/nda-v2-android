import React, {Component} from 'react';
import  {
    View,
    Image,
    ActivityIndicator,
    AsyncStorage,
} from 'react-native';
import LandingPage from './LandingPage'
import Globals from './Globals.js'
import api from '../config/api.js'
import styleCss from '../config/css.js'
const ACCESS_TOKEN = 'access_token';
const USER_DATA = 'user_data';
import {BallIndicator} from 'react-native-indicators';
const USER_ID = 'user_id';
export default class Splash extends Component{
    navigate(routeName){
        this.props.navigator.resetTo({
            name:routeName
        })
    }
constructor(props){
    super(props);
    this.state = {
      accessToken: "",
      fetching: true,
    }
  }   
    componentWillMount() {
        //AsyncStorage.removeItem(ACCESS_TOKEN), AsyncStorage.removeItem(USER_ID)
        this.getToken();
    }

     async storeToken(accessToken){
        try {
            await AsyncStorage.setItem(ACCESS_TOKEN,accessToken)
        }catch ( error) {
            console.log("something went wrong 2 ",error)
        }
    }

    async getToken(token) {     
        try {       
            let accessToken = await AsyncStorage.getItem(ACCESS_TOKEN);
            console.log("getting token done: " + accessToken);
            if(!accessToken) {           
                this.navigate('LandingPage');
            } else {
                try {
                    await api.getUser().then((res) =>{
                        if(res.status == true){
                            Globals.UserEmail = res.data.user.email
                            this.storeToken(accessToken);
                            this.navigate('Dashboard')                    
                            //this.navigate('LandingPage')                    
                        }else{
                            AsyncStorage.removeItem(ACCESS_TOKEN)
                            this.navigate('LandingPage');                 
                        }
                    })
                } catch(e) {
                    console.log("error fetching: ", e);  
                }
            }
        } catch(e) {
            console.log("getting token error",e);
        }
    }  
  async deleteToken() {
    try {
        await AsyncStorage.removeItem(ACCESS_TOKEN)
    } catch(e) {
        console.log("Something went wrong token not deleted",e);
    }
  }

    render(){
        return(
            <View style={styleCss.splashContainer}>
                    <Image
                        style={styleCss.splashLogo}
                        source={require('../images/nda_logo.png')}
                        />
                    <BallIndicator    color="#2296f2" size={20}/>
            </View>   
        );
    }
}


