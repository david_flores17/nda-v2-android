module.exports = {
  name: 'Nda Now',
  ndaParameters:{},
  noticeParties:{},
  noImage:'http://www.freeiconspng.com/uploads/no-image-icon-23.jpg',
  UserEmail:'',
  userState:0,
  updateNdaDashboard:false,
  updateNdaAccount:false,
  updateNdaNoticeParties:false,
  ndaOffset:0, //endless scroll index
  ndaLimit:60,  //endless scroll, # of ndas per api call   
  notificationsOffset:0, //endless scroll index
  notificationsLimit:20,  //endless scroll, # of ndas per api call 
  ndaForgotPasswordLink:"https://beta.app.myndanow.com/forgot_password",
  ndaPricingWebpageLink:"https://www.myndanow.com/#pricing",
  ndaMainWebpageLink:"https://www.myndanow.com/",
  webviewLink:"",
  webviewTitle:"",
  months : ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'Novemeber', 'December']  ,     
  
  toCammelCase(str){
    return str.split('').map(function(word,index){
      if(index == 0){
        return word.toUpperCase();
      }
      return word.charAt(0).toLowerCase() + word.slice(1).toLowerCase();
    }).join('');
  }
};