import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    Dimensions,
    Text,
    TextInput,
    Keyboard,
    ScrollView,
    KeyboardAvoidingView,
    Alert,
    Image,
    AsyncStorage
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { Icon } from 'react-native-elements';
import {BallIndicator} from 'react-native-indicators';
import styleCss from '../config/css';
import api from '../config/api';
import Globals from './Globals.js';
import DropdownAlert from 'react-native-dropdownalert';
const _window = Dimensions.get('window');
const ACCESS_TOKEN = 'access_token';
const STORED_EMAIL = '$stored_email';
const USER_ID = 'user_id';
export default class LoginForm extends Component {  
    constructor(props){
        super(props);
        this.state = {
            password:"",
            confirmPassword:"",
            firstName:"",
            lastName:"",
            email:"",
            noticePartyEmail:"",
            companyName:"",
            firstNameError:false,
            lastNameError:false,
            emailError:false,
            noticePartyEmailError:false,
            passwordError:false,
            loading:false,
            apiError:"",
            registerData:{}
        }     
    };
    navigate(routeName){
        this.props.navigator.resetTo({
            name:routeName,
            animated: true
        })
    }
    async storeToken(accessToken,userId){
        try {
            await AsyncStorage.setItem(ACCESS_TOKEN,accessToken)
            await AsyncStorage.setItem(STORED_EMAIL,this.state.email)
            await AsyncStorage.setItem(USER_ID,userId)
        }catch ( error) {
            console.log("something went wrong ",error)
        }
    }
    ifsuccessMessage(){
        if(this.state.loading){
            return(
                <View style={styleCss.successView}>
                    <BallIndicator    color="#2296f2" size={20}/>
                </View>
            )
        }else{
            return null;
        }
    }
    validateEmail(emailField){
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/
        if (reg.test(emailField) == false)
        {
            return false;
        }
        return true;
    }
    async registerUser(){
        this.setState({loading:true});
        this.state.registerData.email = this.state.email
        this.state.registerData.password = this.state.password
        this.state.registerData.firstName = this.state.firstName
        this.state.registerData.lastName = this.state.lastName
        this.state.registerData.companyName = this.state.companyName
        this.state.registerData.noticePartyEmail = this.state.noticePartyEmail
        console.log(this.state.registerData)
        try{
            Keyboard.dismiss();    
            await api.register(this.state.registerData).then((res)=>{
                if(res.status){                    
                    console.log("register:" , res)
                    let accessToken = res.data.token;
                    let user_id = res.data.user.id;
                    Globals.UserEmail = this.state.email
                    console.log("storing token")
                    this.storeToken(accessToken,user_id).then((res)=>{
                        console.log("storing token DONE ",res)
                        setTimeout(()=>{
                            this.setState({loading:false});
                            this.navigate('Dashboard')
                        },3000)       
                    })
                }else{
                    this.setState({loading:false});  
                    alert(res.error.description)
                    this.setState({apiError:res.error.description}) 
                }
            })
        }catch(e){
            this.setState({loading:false});  
            console.log(e)
            alert("Something went wrong please try again later.")            
        }
    }
    validateData(){
        if(this.state.firstName == ""){
            this.dropdown.alertWithType('error', 'Name:', "Your name can't be blank.");
        }else if(this.state.lastName == ""){
            this.dropdown.alertWithType('error', 'Last Name:', "Your last name can't be blank.");
        }else if(this.state.email == ""){
            this.dropdown.alertWithType('error', 'Email:', "Your email address can't be blank.");
        }else if(!this.validateEmail(this.state.email)){
            this.dropdown.alertWithType('error', 'Email:', "Your email address is not valid.");
        }else if(this.state.password == ""){
            this.dropdown.alertWithType('error', 'Password:', "Your password can't be blank.");
        }else if(this.state.password !== this.state.confirmPassword && this.state.password !== ""){
            this.dropdown.alertWithType('error', 'Password:', "Your password doesn't match.");
        }else if(this.state.noticePartyEmail !== "" && !this.validateEmail(this.state.noticePartyEmail)){
            this.dropdown.alertWithType('error', 'Email:', "Your notice party email address is not valid.");
        }
        // console.log("notice party: ", this.state.noticeParty, "valid email: "+this.state.noticeParty+" "+this.validateEmail(this.state.noticeParty))
        if(this.state.firstName == ""){
            this.state.firstNameError = true
            this.setState({firstNameError : true})
        }else{
            this.state.firstNameError = false
            this.setState({firstNameError : false})            
        }
        if(this.state.lastName == ""){
            this.state.lastNameError = true
            this.setState({lastNameError : true})
        }else{
            this.state.lastNameError = false
            this.setState({lastNameError : false})
        }
        if(this.state.email == ""){
            this.state.emailError = true
            this.setState({emailError : true})
        }else{
            if(this.validateEmail(this.state.email)){
                this.state.emailError = false
                this.setState({emailError : false})
            }else{
                this.state.emailError = true
                this.setState({emailError : true})
            }
        }
        if(this.state.password == ""){
            this.state.passwordError = true
            this.setState({passwordError : true})
        }else{
            if(this.state.password !== this.state.confirmPassword){
                this.state.passwordError = true
                this.setState({passwordError : true})
            }else{
                this.state.passwordError = false
                this.setState({passwordError : false})
            }
        }
        if(this.state.noticePartyEmail !== "" && !this.validateEmail(this.state.noticePartyEmail)){
            this.state.noticePartyEmailError = true
            this.setState({noticePartyEmailError:true})
        }else{
            this.state.noticePartyEmailError = false
            this.setState({noticePartyEmailError : false})
        }
        if(!this.state.firstNameError && !this.state.lastNameError && !this.state.emailError && !this.state.passwordError && !this.state.noticePartyEmailError){
            this.registerUser()
            // alert("OK to go!")
        }
    }    
    render(){
        return(
            <View style={[styleCss.mainViewContainer,{backgroundColor:"#e8e8e8"}]}>
                <View style={styleCss.sendNDAHeader}>
                    <View style={styleCss.iconHeaderContainer}>
                        <Icon type={'ionicon'} name={"ios-arrow-round-back-outline"} size={35} color={"#000"} onPress={()=>this.props.navigator.jumpBack()}/>                     
                    </View>
                    <View style={ styleCss.titleHeaderContainer}>
                        <Text  style={[styleCss.headerText,{width:_window.width-100,left:-10}]}>Register</Text>
                    </View>
                    <View style={styleCss.iconHeaderContainer}>                 
                        <Icon type={'ionicon'} name={"ios-paper-plane-outline"} size={20} color={"#000"} onPress={()=>this.validateData()}/>                     
                    </View>
                </View>
                <KeyboardAwareScrollView 
                    keyboardShouldPersistTaps={'handled'}>    
                    <View style={[styleCss.sendNDA2TitleContainer,styleCss.row]} >
                        <Text  onPress={()=>Keyboard.dismiss()}>FIRST NAME:</Text>
                        {this.state.firstNameError ? <Image style={styleCss.smallImageError} source={require('../images/asteriskRed.png')}/> : null}
                    </View>
                    <View style={styleCss.inputContainer}>  
                        <TextInput
                            clearButtonMode='while-editing'
                            style={this.state.firstNameError ? styleCss.formInputError : styleCss.formInput}
                            editable = {true}
                            placeholder="My First Name"
                            returnKeyType="next"
                            autoCapitalize="words"
                            autoCorrect={false}
                            onSubmitEditing={()=>this.lastNameInput.focus()}
                            onChangeText ={(text)=>this.setState({firstName:text})}
                            ref={(input) => this.firstNameInput=input}/>
                    </View>
                    <View style={[styleCss.sendNDA2TitleContainer,styleCss.row]}>
                        <Text >LAST NAME:</Text>
                        {this.state.lastNameError ? <Image style={styleCss.smallImageError} source={require('../images/asteriskRed.png')}/> : null}                                        
                    </View>
                    <View style={styleCss.inputContainer}>  
                        <TextInput
                            clearButtonMode='while-editing'
                            style={this.state.lastNameError ? styleCss.formInputError : styleCss.formInput}
                            editable = {true}
                            placeholder="My Last Name"
                            returnKeyType="next"
                            autoCapitalize="words"
                            autoCorrect={false}
                            onSubmitEditing={()=>this.emailInput.focus()}    
                            onChangeText ={(text)=>this.setState({lastName:text})}                                                    
                            ref={(input) => this.lastNameInput=input}/>
                    </View>
                    <View style={[styleCss.sendNDA2TitleContainer,styleCss.row]}>
                        <Text >EMAIL:</Text>
                        {this.state.emailError ? <Image style={styleCss.smallImageError} source={require('../images/asteriskRed.png')}/> : null}                                       
                    </View>
                    <View style={styleCss.inputContainer}>  
                        <TextInput
                            clearButtonMode='while-editing'
                            style={this.state.emailError ? styleCss.formInputError : styleCss.formInput}
                            editable = {true}
                            placeholder="myemail@domain.com"
                            keyboardType="email-address"
                            returnKeyType="next"
                            autoCapitalize="none"
                            autoCorrect={false}
                            onChangeText ={(text)=>this.setState({email:text})}                                                                                
                            onSubmitEditing={()=>this.phoneInput.focus()}  
                            ref={(input) => this.emailInput=input}/>
                    </View>
                    <View style={[styleCss.sendNDA2TitleContainer,styleCss.row]}>
                        <Text onPress={()=>Keyboard.dismiss()}>NOTICE PARTY:</Text>
                    </View>
                    <View style={styleCss.inputContainer}>  
                        <TextInput
                            clearButtonMode='while-editing'
                            style={ this.state.noticePartyEmailError ? styleCss.formInputError : styleCss.formInput}
                            editable = {true}
                            placeholder="mynoticeparty@domain.com (Optional)"
                            //keyboardType=""                            
                            returnKeyType="next"
                            autoCapitalize="none"
                            autoCorrect={false}
                            onChangeText ={(text)=>this.setState({noticePartyEmail:text})}   
                            onSubmitEditing={()=>this.companyInput.focus()}  
                            ref={(input) => this.phoneInput=input}/>
                    </View>
                    <View style={[styleCss.sendNDA2TitleContainer,styleCss.row]}>
                        <Text >COMPANY NAME:</Text>
                    </View>
                    <View style={styleCss.inputContainer}>  
                        <TextInput
                            clearButtonMode='while-editing'
                            style={ styleCss.formInput}
                            editable = {true}
                            placeholder="My Company Name (Optional)"
                            returnKeyType="next"
                            autoCapitalize="words"
                            autoCorrect={true}
                            onSubmitEditing={()=>this.passwordInput.focus()}    
                            onChangeText ={(text)=>this.setState({companyName:text})}                                                    
                            ref={(input) => this.companyInput=input}/>
                    </View>
                    <View style={[styleCss.sendNDA2TitleContainer,styleCss.row]}>
                        <Text onPress={()=>Keyboard.dismiss()}>PASSWORD:</Text>
                        {this.state.passwordError ? <Image style={styleCss.smallImageError} source={require('../images/asteriskRed.png')}/> : null}                                        
                    </View>
                    <View style={styleCss.inputContainer}>  
                        <TextInput
                            secureTextEntry
                            clearButtonMode='while-editing'
                            style={this.state.passwordError ? styleCss.formInputError : styleCss.formInput}
                            editable = {true}
                            placeholder="******"
                            returnKeyType="next"
                            autoCapitalize="none"
                            autoCorrect={false}
                            onSubmitEditing={()=>this.confirmPasswordInput.focus()}  
                            onChangeText = {(text)=> this.setState({password: text})}
                            ref={(input) => this.passwordInput=input}/>
                    </View>
                    <View style={[styleCss.sendNDA2TitleContainer,styleCss.row]}>
                        <Text onPress={()=>Keyboard.dismiss()}>CONFIRM PASSWORD:</Text>
                        {this.state.passwordError ? <Image style={styleCss.smallImageError} source={require('../images/asteriskRed.png')}/> : null}                                      
                    </View>
                    <View style={styleCss.inputContainer}>  
                        <TextInput
                            secureTextEntry
                            clearButtonMode='while-editing'
                            style={this.state.passwordError ? styleCss.formInputError : styleCss.formInput}
                            editable = {true}
                            placeholder="******"
                            returnKeyType="go"
                            autoCapitalize="none"
                            autoCorrect={false}
                            onChangeText = {(text)=> this.setState({confirmPassword: text})}                                
                            onSubmitEditing={()=>(console.log("pushed"),this.validateData())}                                  
                            ref={(input) => this.confirmPasswordInput=input}/>
                    </View>
                    <View style={{height:16}}/>
                </KeyboardAwareScrollView>   
                <DropdownAlert 
                    ref={ref => this.dropdown = ref} 
                    imageSrc={require("../images/alertIcon.png")}
                    containerStyle={styleCss.customAlertContainerNdaName}
                    closeInterval={2000}
                />     
                {this.ifsuccessMessage()}
            </View>
        );
    }
}
