import React, {Component} from 'react'; 
import  {
    View,
    ScrollView,
    Text,
    StyleSheet,
    Dimensions,
    FlatList,
    TouchableWithoutFeedback,
    TextInput,
    Alert,
    KeyboardAvoidingView,
    Keyboard,
    TouchableOpacity
        } from 'react-native';
import AutoScroll from 'react-native-auto-scroll'
import DatePicker from 'react-native-datepicker'
import { Dropdown } from 'react-native-material-dropdown';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { Switch } from 'react-native-switch';        
import { NavigationActions } from 'react-navigation'
import { Icon } from 'react-native-elements';
import styleCss from '../config/css';
import Globals from './Globals.js'
import api from '../config/api';
import DropdownAlert from 'react-native-dropdownalert';
import {BallIndicator} from 'react-native-indicators';
import moment from 'moment'
const _window = Dimensions.get('window');
export default class SendNDA1 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loaded:false,
            ndaParametersToSend:this.props.navigation.state.params,
            addThirdParty:false,
            addNoticeParty:false,
            thirdPartyLoaded:true,
            showAlert:false,
            alertTitle:"Error",
            alertText:"The email is not valid.",
            ndaTermLengthArray:[],
            ndaJurisdictionArray:[],
            ndaName:"",
            inviteeEmail:"",
            ndaTermLengthSelected:null,
            startDate:null,
            ndaJurisdictionSelected:null,
            errorText:[],
            noticePartyEmail:"",
            noticePartiesArray:[],
            thirdParties:[],
        }
        setInterval(() => {
            if(Globals.updateNdaNoticeParties){
                console.log('45 UPDATING XXXXXXXXX')
                var tempNoticeParties = []
                Globals.noticeParties.map((details,index)=>{
                    tempNoticeParties.push({
                        id:details.id,
                        value: details.email
                    })
                })                
                this.setState({noticePartiesArray : tempNoticeParties})
                Globals.updateNdaNoticeParties = false
                console.log('uploaded succefully')
            }
        }, 500); 
    }
    validateNdaParameters(){
        this.state.errorText = []
        var error = ""
        if(this.state.ndaName == ""){
            error ="Nda name"
            this.state.errorText.push({
                error
            }) 
        }
        if(this.state.inviteeEmail == ""){
            error ="Invitee email"
            this.state.errorText.push({
                error
            }) 
        }else{
            if(!this.validateEmail(this.state.inviteeEmail)){
                error ="Invitee email is wrong"
                this.state.errorText.push({
                    error
                }) 
            }
        }
        if(this.state.startDate == null){
            var tempDate = Date.now()
            this.state.startDate = moment(tempDate).format("YYYY-MM-DD")
        }
        if(this.state.errorText.length == 0){
            var tempThirdParties = []
            var tempEmail = ""
            this.state.thirdParties.map((details)=>{
                tempEmail = details.email
                tempThirdParties.push(
                    tempEmail
                )
            })
            
            this.state.ndaParametersToSend.name = this.state.ndaName
            this.state.ndaParametersToSend.inviteeEmail = this.state.inviteeEmail
            this.state.ndaParametersToSend.ndaTermLength = this.state.ndaTermLengthSelected.toString()
            this.state.ndaParametersToSend.startDate = moment(this.state.startDate).format("YYYY-MM-DD")
            this.state.ndaParametersToSend.jurisdiction = this.state.ndaJurisdictionSelected.toString()
            this.state.ndaParametersToSend.thirdParties = this.state.addThirdParty ? tempThirdParties: []

            this.state.ndaParametersToSend.noticeParty = this.state.noticeParty
            this.props.navigation.navigate('SendNDA3',this.state.ndaParametersToSend)
        }else{
                let tempError =""
                this.state.errorText.map((details)=>{
                    tempError += details.error +", "
                })
                tempError = tempError.slice(0, tempError.length - 2)
                tempError += "."
            Alert.alert(
                'Please check the missing info:',
                tempError , [
                {text: 'OK', onPress: ()=> null},//BackAndroid.exitApp(),
                {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
            ]);  
        }
    }
    deleteThirdParty(id){
        this.setState({thirdPartyLoaded:false})
        setTimeout(()=>{
            this.state.thirdParties.splice(id,1)
            this.state.thirdParties.map((details,index)=>{
                details.id = index
            })
            this.setState({thirdPartyLoaded:true})
        },500)
    }
    renderThirdParties(item){
        return(
            <View style={[styleCss.thirdPartiesContainer,styleCss.row]}>
                <Text  onPres={()=>Keyboard.dismiss()}style={styleCss.thirdPartiesText}>{item.email}</Text>
                <Icon type={'simple-line-icon'} name={'close'} size={18} color={"#000"} onPress={()=>this.deleteThirdParty(item.id)}/>                                         
            </View>
        )
    }
    mapThirdParties(){
        if(this.state.thirdParties.length>0){
            if(this.state.thirdPartyLoaded){
                return(
                    <FlatList
                        data={( this.state.thirdParties)}
                        renderItem={({item}) => (this.renderThirdParties(item))} 
                        keyExtractor={(item)=>item.id.toString()}   
                    />
                )
            }else{
                return(
                    <View style={styleCss.sendNDA2TitleContainer}>  
                        <BallIndicator color="#2296f2" size={20}/>       
                    </View>           
                )
            }
        }
    }
    validateEmail(emailField){
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/
        if (reg.test(emailField) == false)
        {
            return false;
        }
        return true;
    }
    addThirdParty(email){        
        if(this.validateEmail(email)){
            this.setState({
                thirdPartyLoaded:false,
            })
            this.state.thirdParties.push({
                id:this.state.thirdParties.length,
                email:email
            })
            setTimeout(()=>{
                this.setState({
                    thirdParties:this.state.thirdParties,
                    thirdPartyLoaded:true,
                })
                this.scrollView.scrollToEnd({animated:true})
            },500)
        }else{
            alert("Ivalid third party email address")
        }
    }
    noticeParty(){
        console.log()
        if(this.state.addNoticeParty){
            if(this.state.noticePartiesArray.length > 0){
                return(
                    <View style={styleCss.addNoticePartyContainer}>
                        <View style={[styleCss.addNoticePartyDropdownContainer]}>
                            <Dropdown
                                onPress={()=>Keyboard.dismiss()}
                                dropdownOffset = {{top:0,left:0}}
                                baseColor = {"#FFF"}
                                textColor = {"#BDBDBD"}
                                selectedItemColor = {"black"}
                                value = {this.state.noticePartiesArray[0].value}
                                data = {this.state.noticePartiesArray}
                                onChangeText = {(value,item,index)=>(console.log("value,item,index ",value,item,index))}
                                overlayStyle = {{backgroundColor:"rgba(0,0,0,0.75)",}} 
                                containerStyle = {styleCss.dropdown}
                            />
                        </View>
                        <View style={styleCss.addNoticePartyButtonContainer}>
                            <Icon type={'simple-line-icon'} name={'plus'} size={18} color={"#000"} onPress={()=>{this.props.navigation.navigate('AddNoticeParty')}}/>                                                                 
                        </View>
                    </View>
                )
            }else{
                return(
                    <View style={styleCss.addNoticePartyContainer}>
                            <View style={[styleCss.addNoticePartyDropdownContainer,{justifyContent:"center"}]}>
                                <Text style={styleCss.thirdPartiesText}>Empty</Text>
                            </View>
                            <View style={styleCss.addNoticePartyButtonContainer}>
                                <Icon type={'simple-line-icon'} name={'plus'} size={18} color={"#000"} onPress={()=>{this.props.navigation.navigate('AddNoticeParty')}}/>                                                                 
                            </View>
                        </View>
                )
            }
        }else{
            return null;
        }
    }
    thirdParties(){
        if(this.state.addThirdParty){
            return(
                <View >  
                    <View style={styleCss.inputContainer}>
                        <TextInput
                            clearButtonMode='while-editing'
                            style={styleCss.formInput}
                            editable = {true}
                            placeholder="myname@mydomain.com"
                            keyboardType="email-address"
                            returnKeyType="go"
                            autoCapitalize="none"
                            autoCorrect={false}
                            onSubmitEditing={()=>( this.addThirdParty(this.thirdPartyInput._lastNativeText))}
                            //onChangeText={(text) => this.setState({noticePartyEmail:text})}
                            ref={(input)=>this.thirdPartyInput=input}/>
                        {this.mapThirdParties()}
                    </View>
                </View>    
            )
        }else{
            return null;
        }
    }
    showHint(type){
        if(type == "ndaName"){
            this.dropdown.alertWithType('custom', 'NDA Name:', "The nickname of this NDA to differentiate with others.");
        }else if(type == "inviteeEmail"){
            this.dropdown.alertWithType('custom', 'Invitee Email:', "Who is receiving this NDA as invitee.");            
        }else if(type == "thirdParty"){
            this.dropdown.alertWithType('custom', 'Invitors Notice Party:', "The party you designate to receive notices for the invitor pursuant to the terms of the agreement.");            
        }else if(type == "termLength"){
            this.dropdown.alertWithType('custom', 'Term Length:','The party you designate to receive notices for the invitor pursuant to the terms of the agreement.');            
        }else if(type == "startDate"){
            this.dropdown.alertWithType('custom', 'Start Date:', "Beginning of NDA terms.");            
        }else if(type == "jurisdictionStates"){
            this.dropdown.alertWithType('custom', 'Jurisdiction:', "Jurisdiction state.");            
        }else if(type == "noticeParty"){
            this.dropdown.alertWithType('custom', 'Notice Party:', "The party you designate to receive notices for the invitor pursuant to the terms of the agreement.");            
        }
    }
    ifReadyToNextScreen(){
        if(this.validateEmail(this.state.inviteeEmail) && !this.state.ndaName == ""){
            return(
                <Icon type={'ionicon'} name={"ios-arrow-round-forward-outline"} size={35} color={"#000"} onPress={()=> this.validateNdaParameters()}/>                                 
            )            
        }else{
            return(
                <Icon type={'ionicon'} name={"ios-arrow-round-forward-outline"} size={35} color={"#BDBDBD"} onPress={()=> this.validateNdaParameters()}/>                                                      
            )
        }
    }
    componentWillMount(){
        var tempTermLength = []
        Globals.ndaParameters.agreement_terms.map((details,index)=>{
            tempTermLength.push({
                id:details.id,
                value: details.term
            })
        })
        this.state.ndaTermLengthArray = tempTermLength
        this.state.ndaTermLengthSelected = tempTermLength[0].id

        var tempJurisdictions = []
        Globals.ndaParameters.jurisdiction_states.map((details,index)=>{
            tempJurisdictions.push({
                id:details.id,
                value: details.name
            })
        })
        this.state.ndaJurisdictionArray = tempJurisdictions
        this.state.ndaJurisdictionSelected = tempJurisdictions[0].id

        var tempNoticeParties = []
        Globals.noticeParties.map((details,index)=>{
            tempNoticeParties.push({
                id:details.id,
                value: details.email
            })
        })
        this.state.noticePartiesArray = tempNoticeParties
    }
    render(){
        return(
            <View style={[styleCss.mainViewContainer,{backgroundColor:"#e8e8e8"}]}>
                   <View style={[styleCss.sendNDAHeader,{paddingTop:29, paddingBottom:10}]}>
                        <View style={[{height:44,paddingBottom:20}]}>
                            <Icon type={'ionicon'} name={"ios-arrow-round-back-outline"} size={35} color={"#000"} onPress={()=>this.props.navigation.goBack()}/>                     
                        </View>
                        <View style={ [styleCss.headerTextContainer,{marginRight:-1,marginLeft:-19}]}>
                            <Text style={styleCss.headerText}>Step 2 of 3</Text>
                        </View>
                        <View style={[{height:44,paddingBottom:20}]}>                 
                            {this.ifReadyToNextScreen()}
                        </View>
                    </View>
                    <KeyboardAwareScrollView
                        onPress={()=>Keyboard.dismiss()}
                        ref={ref => this.scrollView = ref}>
                        <View style={[styleCss.sendNDA2TitleContainer,styleCss.row]}>
                            <Text onPress={()=>Keyboard.dismiss()}>NDA NAME:</Text>
                            <Icon type={'simple-line-icon'} name={'question'} size={18} color={"#000"} onPress={()=>{this.showHint('ndaName')}}/>                                         
                        </View>
                        <View style={styleCss.inputContainer}>
                            <TextInput
                                clearButtonMode='while-editing'
                                style={styleCss.formInput}
                                editable = {true}
                                placeholder="My Amazing NDA"
                                returnKeyType="next"
                                autoCapitalize="words"
                                autoCorrect={true}
                                onChangeText={(text) => this.setState({ndaName:text})}
                                onSubmitEditing={()=>this.emailInput.focus()}
                                ref={(input)=>this.ndaNameInput=input}
                                />
                        </View>
                        <View style={[styleCss.sendNDA2TitleContainer,styleCss.row]}>
                            <Text onPress={()=>Keyboard.dismiss()}>INVITEE EMAIL ADDRESS:</Text>
                            <Icon type={'simple-line-icon'} name={'question'} size={18} color={"#000"} onPress={()=>{this.showHint('inviteeEmail')}} />                                         
                        </View>
                        <View style={styleCss.inputContainer}>
                            <TextInput
                                clearButtonMode='while-editing'
                                style={styleCss.formInput}
                                editable = {true}
                                placeholder="myname@mydomain.com"
                                keyboardType="email-address"
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(text) => this.setState({inviteeEmail:text})}
                                onSubmitEditing={()=>this.state.addThirdParty?this.thirdPartyInput.focus():null}
                                ref={(input)=>this.emailInput=input}/>
                        </View>     
                        <View style={[styleCss.sendNDA2TitleContainer,styleCss.row]}>
                            <Text onPress={()=>Keyboard.dismiss()}>TERM LENGTH:</Text>
                            <Icon type={'simple-line-icon'} name={'question'} size={18} color={"#000"} onPress={()=>{this.showHint('termLength')}}/>                                         
                        </View>
                        <View style={styleCss.dropdownContainer}>
                            <Dropdown
                                dropdownOffset = {{top:0,left:0}}
                                baseColor = {"#FFF"}
                                textColor = {"#BDBDBD"}
                                selectedItemColor = {"black"}
                                value = {this.state.ndaTermLengthArray[0].value}
                                data = {this.state.ndaTermLengthArray}
                                onChangeText = {(value,item,index)=>(this.state.ndaTermLengthSelected = Globals.ndaParameters.agreement_terms[(item)].id)}
                                overlayStyle = {{backgroundColor:"rgba(0,0,0,0.75)",}} 
                                containerStyle = {styleCss.dropdown}
                            />
                        </View>
                        <View style={[styleCss.sendNDA2TitleContainer,styleCss.row]}>
                            <Text onPress={()=>Keyboard.dismiss()}>START DATE:</Text>
                            <Icon type={'simple-line-icon'} name={'question'} size={18} color={"#000"} onPress={()=>{this.showHint('startDate')}}/>                                         
                        </View>
                        <DatePicker
                            style={{width: "100%", backgroundColor:"#FFF",bottom:0}}
                            date={this.state.startDate}
                            mode="date"
                            placeholder="Start Date"
                            format="MMMM DD, YYYY"
                            showIcon={false}
                            //minDate="2018-05-01"
                            //maxDate="2016-06-01"
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            customStyles={{
                                dateText:{
                                    color:"#BDBDBD",
                                    fontSize:17
                                },
                                placeholderText:{
                                    color:"#BDBDBD",
                                    fontSize:17
                                },
                                dateInput: {
                                    marginLeft: 16,
                                    borderWidth:0,
                                    alignItems:"flex-start",
                                    backgroundColor:"#FFF"
                                }
                            }}
                            onDateChange={(date) => {this.setState({startDate: date}),(this.state.startDate)}}
                        />
                        <View style={[styleCss.sendNDA2TitleContainer,styleCss.row]}>
                            <Text onPress={()=>Keyboard.dismiss()}>JURISDICTION:</Text>
                            <Icon type={'simple-line-icon'} name={'question'} size={18} color={"#000"} onPress={()=>{this.showHint('jurisdictionStates')}}/>                                         
                        </View>
                        <View style={styleCss.dropdownContainer}>
                            <Dropdown
                                dropdownOffset={{top:0,left:0}}
                                baseColor={"#FFF"}
                                textColor={"#BDBDBD"}
                                selectedItemColor={"black"}
                                value={this.state.ndaJurisdictionArray[0].value}
                                data={this.state.ndaJurisdictionArray}
                                onChangeText={(value,item,index)=>this.state.ndaJurisdictionSelected = (item)}
                                overlayStyle={{backgroundColor:"rgba(0,0,0,0.75)",}}
                                containerStyle = {styleCss.dropdown}                                
                                pickerStyle={{top:150}}
                                itemCount={10}
                            />
                        </View>
                        <View style={[styleCss.sendNDA2TitleContainer,styleCss.row]}>
                            <Text onPress={()=>Keyboard.dismiss()}>NOTICE PARTY:</Text>
                            <Icon type={'simple-line-icon'} name={'question'} size={18} color={"#000"}  onPress={()=>{this.showHint('noticeParty')}}/>                                         
                        </View>
                        <View style={styleCss.switchContainer}>
                            <Switch
                                circleSize={19}
                                barHeight={19}
                                circleBorderWidth={0}
                                backgroundActive={'#e8e8e8'}
                                backgroundInactive={'#e8e8e8'}
                                circleActiveColor={'#2296f2'}
                                circleInActiveColor={'#808080'}
                                inerCircleStyle={{borderWidth:3}}
                                onValueChange={(value) => (this.setState({addNoticeParty: value}), value ? this.scrollView.scrollToEnd({animated: true}) : null)}
                                value={this.state.addNoticeParty}/>
                        </View>
                        {this.noticeParty()}
                            <View style={[styleCss.sendNDA2TitleContainer,styleCss.row]}>
                                <Text onPress={()=>Keyboard.dismiss()}>ADD NOTICE THIRD PARTIES?:</Text>
                                <Icon type={'simple-line-icon'} name={'question'} size={18} color={"#000"}  onPress={()=>{this.showHint('thirdParty')}}/>                                         
                            </View>
                            <View style={styleCss.switchContainer}>
                                <Switch
                                    circleSize={19}
                                    barHeight={19}
                                    circleBorderWidth={0}
                                    backgroundActive={'#e8e8e8'}
                                    backgroundInactive={'#e8e8e8'}
                                    circleActiveColor={'#2296f2'}
                                    circleInActiveColor={'#808080'}
                                    inerCircleStyle={{borderWidth:3}}
                                    onValueChange={(value) => (this.setState({addThirdParty: value}),value ? this.scrollView.scrollToEnd({animated: true}) : null)}
                                    value={this.state.addThirdParty}/>
                                <View style={ this.state.addThirdParty ? {height:0}:{height:32}}/>                                                
                            </View>
                            {this.thirdParties()}                            
                    </KeyboardAwareScrollView>
                    <DropdownAlert 
                        ref={ref => this.dropdown = ref} 
                        imageSrc={require("../images/information.png")}
                        containerStyle={styleCss.customAlertContainerNdaName}
                        closeInterval={8000}
                    />
            </View>
        )    
    }
}
