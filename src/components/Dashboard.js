import React, {Component} from 'react';
import  {
    View,
    StyleSheet,
    Image,
    TouchableOpacity,
    TextInput,
    ScrollView,
    ListView,
    KeyboardAvoidingView,
    TouchableHighlight,
    Dimensions,
    Text,
    AsyncStorage,
    RefreshControl,
    AppState,
    Alert,
    FlatList
} from 'react-native'; 
import {BallIndicator} from 'react-native-indicators';
import {Icon} from 'react-native-elements';
import { NavigationActions } from 'react-navigation'
import RNRestart from 'react-native-restart';
import {List, ListItem,SearchBar} from 'react-native-elements' 
import api from '../config/api';
import styleCss from '../config/css';
import Globals from './Globals.js'
import firebase from 'react-native-firebase'
const _window = Dimensions.get('window');
const USER_ID = 'user_id';
const ACCESS_TOKEN = 'access_token';
const DEVICE_TOKENIOS = '@$device_token_ios';
const isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
  const paddingToBottom = 20;
  return layoutMeasurement.height + contentOffset.y >=
    contentSize.height - paddingToBottom;
};
export default class Dashboard extends Component{
    constructor(props){
        super(props)
        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})
        this.state={
            isRefreshing:false  ,
            loaded:false,
            ndaList:[],
            allNdas:[],
            ndasBackup:[],
            deletingNda:false,
            _color:'',
            dataSource: new ListView.DataSource({
                rowHasChanged: (r1, r2) => r1 !== r2,                    
            }),
            isEmpty:false, 
            text:'',      
            bottomRefreshing : false,
            permissionRequested: false,
            endReached:false
        };
        setInterval(() => {
            //console.log('44 Checking for updates should?:',Globals.updateNdaDashboard)
            if(Globals.updateNdaDashboard){
                console.log('45 UPDATING XXXXXXXXX')
                Globals.ndaOffset = 0
                this.getNdaList()
                Globals.updateNdaDashboard=false
                console.log('uploaded succefully')
            }else{
                //console.log('53 nothing to update')
            }
        }, 5000);
  
    }
    navigate(routeName){
        this.navigator.resetTo({
            name:routeName
        })
    }
    iconType(status){
        if(status=='Pending'){
            return('clock')
        }
         if(status=='Refused'){
            return('close-o')
        }
         if(status=='Accepted'){
           return('check')
        }
    }
    colorSet(status){
        if(status=='Pending'){
            return('#FF9500')
        }
         if(status=='Refused'){
            return('#D0011B')
        }
         if(status=='Accepted'){
           return('#5FB700')
        }
    }
    ndaIcon(rowData){   
        return(
            <Icon style={{paddingRigth:16}} type={'evilicon'} name={this.iconType(rowData.status)} size={25} color={this.colorSet(rowData.status)}/>                     
        )
    }
    renderRow(rowData){
        return (     
            <TouchableHighlight style={{marginTop:16,marginHorizontal:16}}onPress={() => this.onLearnMore(rowData)}>
                <View style={{backgroundColor:'white',flexDirection:"row",justifyContent:"space-between"}}>
                    <View  style={{maxWidth:_window.width*.8}}>
                        <Text numberOfLines={1} style={{paddingTop:16,paddingHorizontal:16}}>{rowData.name ? rowData.name : 'No title provided'}</Text>
                        <Text numberOfLines={1} style={{marginTop:10,paddingBottom:16,paddingHorizontal:16,fontSize:10}}>{rowData.invitee ? (rowData.invitee.first_name+" "+rowData.invitee.last_name) : rowData.agreement_invite.email}</Text>       
                    </View>
                    <View style={{marginRight:16,alignItems:"center",justifyContent:"center"}}>
                        {this.ndaIcon(rowData)}                             
                    </View>
                </View>     
            </TouchableHighlight>
        )
    }

    ifLoadingMoreNdas(){
        if(this.state.bottomRefreshing){
            return(
             <View style={styleCss.loadingTopContainer}>          
                <BallIndicator    color="#2296f2" size={20}/>
                <Text style={styleCss.loadingTopText}>
                    Loading.....
                </Text>              
            </View>
            )
        }else{
            return null;
        }
    }
    renderLoadingView(){
       return(
            <View style={styleCss.loadingViewContainer}>          
                <Text style={styleCss.loadingTopText}>
                    Loading.....
                </Text>     
                <BallIndicator color="#2296f2" size={20}/>
            </View>
       )
    }
    renderEmptyView(){
        return(
            <ScrollView 
                refreshControl={
                    <RefreshControl
                        refreshing={this.state.isRefreshing}
                        onRefresh={this._onRefresh.bind(this)}
                        tintColor="#3498db"
                        title="Loading..."
                        colors={['#3498db', '#6699ff', '#3377ff']}
                        progressBackgroundColor="#ccddff"/>
                }>
                <TouchableOpacity style={styleCss.DashboardEmptyViewContainer} onPress={() => this.props.navigation.navigate('SendNDA1')}>
                        <Text numberOfLines={1} style={{paddingBottom:10}}>Create Your First NDA</Text>
                        <Text numberOfLines={1} style={styleCss.linkText}>Click Here</Text>
                </TouchableOpacity>
            </ScrollView>    
        )
    }
    async feedEndlessScroll(){
        console.log("text ","'"+this.state.text+"'")
        if(!this.state.bottomRefreshing && !this.state.endReached && !this.state.isRefreshing && this.state.text == ''){
            this.setState({bottomRefreshing : true})
            try{
                await api.getNDAS().then((res)=>{
                    if (res.status && res.data.total_length > 0){
                        if(res.data.events_count < Globals.eventsLimit){
                            this.state.endReached = true
                        }
                        this.setState({
                            ndaList : [...this.state.ndaList , ...res.data.list],
                        })
                        Globals.ndaOffset = Globals.ndaOffset + res.data.total_length                    
                    }else{
                        this.state.endReached =true
                    }
                })
            }catch(e){
                this.setState({bottomRefreshing : false})                    
                console.log(e)
            }finally{
                this.setState({
                    bottomRefreshing : false ,
                })    
            }
        }
    }
    renderLoaded(){
        return(
            <View style={[styleCss.mainViewContainer,{backgroundColor:"#e8e8e8"}]}>
             <View style={[styleCss.sendNDAHeader,{paddingTop:29, paddingBottom:10}]}>
                <View style={styleCss.headerTextContainer}>  
                        <Text style={[styleCss.headerText,{marginRight:16}]}>Manage</Text>
                    </View>
                        
                </View>
                <SearchBar        
                    noIcon
                    //icon= {null}
                    lightTheme
                    clearButtonMode='while-editing'     
                    placeholderTextColor={'#93939C'}
                    inputStyle={styleCss.searchBarInput}
                    containerStyle={styleCss.searchBarContainer}
                    onChangeText={(text) => this.filterSearch(text)}
                    placeholder='Search...' 
                    value={this.state.text}/>  
                <ScrollView    
                    onScroll={({nativeEvent}) => {
                        if (isCloseToBottom(nativeEvent)) {
                            this.feedEndlessScroll()
                        }
                    }}
                    scrollEventThrottle={400}                
                    style={{marginBottom:5}}
                    ref={ref => this.ref_A = ref} parent={this}
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.isRefreshing}
                            onRefresh={this._onRefresh.bind(this)}
                            tintColor="#3498db"
                            title="Loading..."
                            colors={['#3498db', '#6699ff', '#3377ff']}
                            progressBackgroundColor="#ccddff" />
                    }>
                    <FlatList   
                        style={{backgroundColor:"#e8e8e8"}}
                        renderItem={({item})=>(this.renderRow(item))}
                        data={(this.state.ndaList)}
                        keyExtractor={(item)=> item.id.toString() }
                        />         
                    <View style={{height:20,backgroundColor:"#e8e8e8",flex:1}}/>
                    {this.ifLoadingMoreNdas()}
                </ScrollView>   
            </View>
        )
    }
     filterSearch(text){
        const newData = this.state.allNdas.filter(function(item){
            const itemData = item.name.toUpperCase()
            const textData = text.toUpperCase()
            return itemData.indexOf(textData) > -1
        })
        this.setState({
            ndaList: newData,
            text: text
        })
    }
    _onRefresh(){
        Globals.ndaOffset = 0
        this.setState({
            isRefreshing: true, 
            text:""
        })
        this.feedList();
    }
    onLearnMore = (nda) => {
        this.props.navigation.navigate('NdaDetail', nda);
    }
    async feedList(){
        await api.getNDAS()
        .then((res)=>{
            Globals.ndaOffset = Globals.ndaOffset + res.data.total_length
            this.setState({
                ndaList :  res.data.list,
                ndasBackup: res.data.list,
                loaded:true,     
            })
        })  
        .then((res)=>{ 
            this.setState({
                isRefreshing:false, 
            })    
        })
    }
    async deleteStoredData(){
        await AsyncStorage.removeItem(ACCESS_TOKEN)
        await AsyncStorage.removeItem(USER_ID)
        RNRestart.Restart()
    }

    async getNdaList(){
        try{
            api.getNDAS().then((res)=>{ 
                console.log(res)
                if(res.status){
                    Globals.ndaOffset = Globals.ndaOffset + res.data.total_length
                    this.setState({ 
                        ndaList :  res.data.list,
                        ndasBackup : res.data.list,
                        isEmpty : false,
                        deletingNda : false
                    })
                    if(res.data.total_length==0){
                        this.setState({ 
                            isEmpty : true,
                            loaded: true
                         })//empty List
                        console.log("empty: ",this.state.isEmpty + " Loaded: " + this.state.loaded )
                    }
                } 
                else{
                    if(res.error.code=="err_token_expired"){
                        this.deleteStoredData()
                    }
                    this.setState({
                        isEmpty : true
                    })
                }  
            })       
            api.getAllNdas().then((res)=>{ 
                if(res.status){
                    this.state.allNdas = res.data.list
                } 
                else{
                    if(res.error.code=="err_token_expired"){
                        this.deleteStoredData()
                    }
                }  
            })      
        }catch(e){
            console.log(e)
            alert('This action failed!, Please check your internet connection!')
        }finally{
            this.setState({loaded : true,})
        }
    }
    async componentDidMount() {
        console.log("dashboard")
        await firebase.messaging().requestPermissions()
        .then((permit) => {
            this.state.permissionRequested = permit.granted
            console.log("granted",this.state.permissionRequested)
            if(this.state.permissionRequested){
                console.log(Globals.orgData.uid)
                firebase.messaging().getToken()
                .then((token) => {
                    console.log("FIREBASE TOKEN",token);
                    AsyncStorage.getItem(DEVICE_TOKENIOS, (err, result) => {
                        if (result !== null) {
                        console.log('TOKEN STORED',token)
                        //console.log('API should have token stored')
                        }else{
                            console.log('NO TOKEN STORED, connecting API ')
                            try{
                                api.registerDevice('ios',token).then((res)=>{
                                    console.log('API res:',res)          
                                    if(res.status){
                                        //firebase.messaging().subscribeToTopic(Globals.orgData.uid) //if subscribe to topic
                                        //firebase.messaging().subscribeToTopic('TESTING_DAVID')                                                 
                                        AsyncStorage.setItem(DEVICE_TOKENIOS, token)
                                    }else{
                                        //console.log("Token was NOT SEND",res ) 
                                    }
                                })
                            }catch(e){
                                console.log(e)
                            }  
                        }  
                    })
                })
            }
        });
    }
    componentWillMount() { 
        console.log(firebase)   
        this.getNdaList()
    }
    render(){ 
        const navigation = this.props.navigation;     
        if(!this.state.loaded){ 
                return this.renderLoadingView();
        }else{   
            if(this.state.isEmpty){
                return this.renderEmptyView();
            }else{
                return this.renderLoaded();
            }
        } 
    }
}

   