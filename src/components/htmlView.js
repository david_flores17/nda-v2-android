import React, { Component } from 'react'
import {
  StyleSheet,
  View,
  Dimensions,
  Text,
  Webview,
  ScrollView
} from 'react-native';
import {Icon} from 'react-native-elements';
const _window = Dimensions.get('window');      
import styleCss from '../config/css.js'
import api from '../config/api';
import Globals from './Globals.js'
import HTML from 'react-native-render-html';
import {BallIndicator} from 'react-native-indicators';
export default class htmlView extends Component {
  constructor(props) {
    super(props);
      console.log(this.props.navigation.state.params)
    this.state = {  
      fullNDA: this.props.navigation.state.params.id,
      html:"<html></html>",
      errorGettingData:false,
      
    };
  }
  renderErrorGetingData(){
    if(this.state.errorGettingData){
      return(
        <View>
          <Text>
            This action failed!, Please try again later!
          </Text>
        </View>
      )
    }else{
      return null;
    }
  }
  componentWillMount(){
    // console.log('this.state.fullNDA.encrypted_id ' ,(this.state.fullNDA))    
    // console.log('encode url:',urlencode(this.state.fullNDA))
    console.log('id:',(this.state.fullNDA))
    try{
      api.getHTML(this.state.fullNDA).then((res)=>{       
        if(res.status){
        console.log('compoenentwillMount',res)
          this.setState({
            html: res.data,
          })
        }else{
          this.setState({errorGettingData:true})
        }
      }) 
    }catch(e){
      console.log(e)
      this.setState({errorGettingData:true})
    }finally{
      this.setState({loaded:true})      
    }
  }
  renderLoaded(){
      return(
        <View style={[styleCss.mainViewContainer,{padding:16}]}>   
          <ScrollView> 
            <HTML html={this.state.html} imagesMaxWidth={_window.width}/>
            {this.renderErrorGetingData()}
          </ScrollView> 
        </View>
      )
  }
  renderLoadingView(){
    return(
      <View style={styleCss.loadingViewContainer}>          
        <Text style={styleCss.loadingTopText}>
            Loading.....
        </Text>     
        <BallIndicator    color="#2296f2" size={20}/>
      </View>      
    )
  }
  render() {
    if(!this.state.loaded){
      return this.renderLoadingView();
    }else{           
      return this.renderLoaded();
    }  
  }
}
