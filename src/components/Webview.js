import React, { Component } from 'react'
import {
  StyleSheet,
  View,
  ScrollView,
  WebView,
  Dimensions,
  Text,
  TouchableOpacity
} from 'react-native';
import {Icon} from 'react-native-elements';
const _window = Dimensions.get('window');      
import styleCss from '../config/css.js'
import Globals from './Globals.js'
export default class Webview extends Component {
  constructor(props) {
    super(props);
      //url: Globals.webviewLink,
        console.log(this.props.navigation.state.params)

    this.state = {  
      url: this.props.navigation.state.params.url,
       title: this.props.navigation.state.params.title,
      // url: 'https://www.google.com'
    };
  }
  render() {
    return (
      <View style={styleCss.mainViewContainer}>   
        <WebView
          source={{uri:this.state.url}}/>
      </View>
    );
  }
}
//this.props.navigator.replacePrevious({name:"LandingPage"})