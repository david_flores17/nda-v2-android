import React, { Component } from 'react';
import {StyleSheet,
        View,
        TextInput,
        TouchableOpacity,
        Text,
        KeyboardAvoidingView,
        Keyboard,
        AsyncStorage,
        Image,
        Dimensions,
        ActivityIndicator,
        ScrollView,
        Platform
    } from 'react-native';
import Dashboard from './Dashboard'
import Globals from './Globals.js';
import styleCss from '../config/css.js';
import api from '../config/api';
const _window = Dimensions.get('window');
const ACCESS_TOKEN = 'access_token';
const STORED_EMAIL = '$stored_email';
const USER_ID = 'user_id';
export default class LoginForm extends Component {  
    navigate(routeName){
        this.props.navigator.resetTo({
            name:routeName,
            animated: true
        })
    }
    async onLoginPressed (){
        if((this.state.email=="")||(this.state.password==""))
        {   
            this.setState({error:"Email and/or Password can't be blank."})            
        }else{   
            Keyboard.dismiss();    
            this.setState({loading:true});
            await api.login(this.state.email,this.state.password)
                .then((res)=>{
                if(res.status){                    
                    console.log("login:" , res)
                    this.setState({error:""})
                    let accessToken = res.data.token;
                    let user_id=res.data.user.id;
                    Globals.UserEmail=this.state.email
                    this.storeToken(accessToken,user_id);
                    api.pick_Company(res.data.companies[0].id).then((res)=>{
                        console.log("pickup_company",res)
                        this.navigate('Dashboard')
                    })
                }else{
                    this.setState({error:"Incorrect Email and/or Password "}) 
                    this.setState({loading:false});  
                }
            })
            .catch((error) => {
                console.error(error)
                this.setState({loading:false});  
            })
        }
    }
    constructor(){
        super();
        this.state={
            email:"",
            password:"",
            error: "",
            loading:false
        }
    }
    async storeToken(accessToken,userId){
        try {
            await AsyncStorage.setItem(ACCESS_TOKEN,accessToken)
            await AsyncStorage.setItem(USER_ID,userId)
            await AsyncStorage.setItem(STORED_EMAIL,this.state.email)
        }catch ( error) {
            console.log("something went wrong ",error)
        }
    }
    async getToken(){
        try {
            let token = await AsyncStorage.getItem(ACCESS_TOKEN);
            console.log("token:   " + token );
        }catch ( error) {
           console.log("something went wrong")
        }
    }
    errorText(){
        if(this.state.error !== ""){
            return(
                <Text style={styleCss.errorText}>     {this.state.error}</Text>            
            )
        }else{
            return null;
        }
    }
    renderLoadingView(){
        if(this.state.loading){
            return(
            <ActivityIndicator 
                size="large"
                color="#2980b9"
                />
            )
        }else{
            return( 
                <View>
                    <TouchableOpacity 
                            style={styleCss.loginButton}
                            onPress={this.onLoginPressed.bind(this)}
                            >
                            <Text style={styleCss.buttonText}>LOGIN</Text>
                        </TouchableOpacity>  
                </View>
            )
        }
    }
    render() {   
        return (
            <ScrollView>
                <KeyboardAvoidingView style={[styleCss.mainViewContainer,Platform.OS==='ios'?{marginTop:_window.height/8}:{paddingTop:_window.height/8}]}>
                    <View style={styleCss.loginLogoContainer}>
                        <Image
                            style={styleCss.loginLogo}
                            source={require('../images/nda_logo.png')}/>
                    </View>
                    <View style={styleCss.loginTextinputsContainer}>
                        <TextInput 
                            clearButtonMode='while-editing'
                            placeholder="  Email Address"  
                            placeholderTextColor={"#93939C"}
                            keyboardType="email-address"
                            returnKeyType="next"
                            onSubmitEditing={()=>this.passwordInput.focus()}
                            autoCapitalize="none"
                            autoCorrect={false}
                            style={ this.state.error == "" ? styleCss.textInput : styleCss.textInputError}
                            onChangeText={(text) => this.setState({email:text})}
                        />
                        <TextInput        
                            clearButtonMode='while-editing'
                            placeholder="  Password"  
                            placeholderTextColor={"#93939C"}
                            returnKeyType="go"
                            secureTextEntry
                            onChangeText={(text) => this.setState({password:text})}
                            style={ this.state.error == "" ? styleCss.textInput : styleCss.textInputError}
                            ref={(input)=>this.passwordInput=input} 
                            onSubmitEditing={()=>this.onLoginPressed()}
                        />  
                    </View>
                    {this.errorText()}
                    <View style={styleCss.loginButtonContainer}>
                        {this.renderLoadingView()}
                        <View style={styleCss.loginGoBackContainer}>
                            <TouchableOpacity onPress={()=>this.props.navigator.jumpBack()}>  
                                <Text style={styleCss.linkText}>{"<< Go Back"}</Text>
                            </TouchableOpacity>
                        </View>
                    </View> 
                </KeyboardAvoidingView>
            </ScrollView>
        );
    }
}
