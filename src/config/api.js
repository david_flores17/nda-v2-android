import {    AsyncStorage } from 'react-native';
import Globals from '../components/Globals.js'
const ndaURL='https://beta.api.myndanow.com/v1/';
const ACCESS_TOKEN = 'access_token';
var api = {
    async login(email,password){  
        return fetch(ndaURL+'users/login', {
            method : 'POST',
            headers : {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body : JSON.stringify({               
                user : email,
                password : password,               
            })
        })
        //.then(isConnected => {isConnected ? null : alert("Not Concected")})    
        .then((res)=>res.json())
    },
    async getUser(){ //.... getting user details using session 
        let accessToken = await AsyncStorage.getItem(ACCESS_TOKEN);
        return fetch(ndaURL+'users/session', {
            method : 'POST',
            headers : {
                'Accept' : 'application/json',
                'Content-Type' : 'application/json',
            },
            body : JSON.stringify({               
                token : accessToken,               
            })
        }).then((res)=>res.json())
          .catch((error) => {console.error(error);
        });       
    }, 

    async pick_Company(_id){ //.... getting companies details using pick_Company
        let accessToken = await AsyncStorage.getItem(ACCESS_TOKEN);
        return fetch(ndaURL+'users/pick_company', {
            method : 'POST',
            headers : {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({               
                token : accessToken,
                id : _id               
            })
        }).then((res)=>res.json())   
          .catch((error) => {console.error(error);})      
    },       

    async sendNDA(ndaParameters){ //.... sending nda 
        let accessToken = await AsyncStorage.getItem(ACCESS_TOKEN);
        return fetch(ndaURL+'agreements/create', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({               
                token : accessToken,
                invitee_email : ndaParameters.inviteeEmail,
                invitor_notify_email :Globals.UserEmail, 
                name : ndaParameters.name,
                start_date : ndaParameters.startDate,
                agreement_type_id :  ndaParameters.type,
                purpose_id : ndaParameters.purpose,
                term_id : ndaParameters.ndaTermLength,
                jurisdiction_id : ndaParameters.jurisdiction,
                bolt_ons : ndaParameters.boltOns,
                third_parties : ndaParameters.thirdParties,
            })
        }).then((res)=>res.json())
          .catch((error) => {console.error("error:::",error);});      
    },       

    async getNDAS(){ //.... getting ndas
        let accessToken = await AsyncStorage.getItem(ACCESS_TOKEN);
        return fetch(ndaURL+'agreements/get_list', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({               
                token : accessToken,  
                offset : Globals.ndaOffset,
                limit : Globals.ndaLimit             
            })
        })
            .then((res)=>res.json())
            .catch((error) => {console.error(error);
        });                 
    },       
    async getAllNdas(){ //.... getting ndas
        let accessToken = await AsyncStorage.getItem(ACCESS_TOKEN);
        return fetch(ndaURL+'agreements/get_list', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({               
                token : accessToken,  
                offset : "0",
                limit : "0"             
            })
        })
            .then((res)=>res.json())
            .catch((error) => {console.error(error);
        });                 
    }, 
    async ndaDetails(nda){ //.... getting nda details
            let accessToken = await AsyncStorage.getItem(ACCESS_TOKEN);
            return fetch(ndaURL+'agreements/by_uuid', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({               
                    token : accessToken,  
                    uuid : nda.uuid,             
                })
            }).then((res)=>res.json())
            .catch((error) => {console.error(error);
        });            
    },
    async acceptNDA(nda){ //.... Accepting nda 
            let accessToken = await AsyncStorage.getItem(ACCESS_TOKEN);
            return fetch(ndaURL+'agreements/accept_nda', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({               
                    token : accessToken,  
                    id : nda.id, 
                    notice_party_email : nda.invitee.email            
                })
            }).then((res)=>res.json())
            .catch((error) => {console.error(error);
        });            
    },
    async refuseNDA(nda){ //.... denying nda 
        let accessToken = await AsyncStorage.getItem(ACCESS_TOKEN);
        return fetch(ndaURL+'agreements/refuse_nda', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({               
                token : accessToken,  
                id : nda.id, 
                           
            })
        }).then((res)=>res.json())
        .catch((error) => {console.error(error);});            
    },


async getNdaParameters(){ //.... creating nda 1/2
        let accessToken = await AsyncStorage.getItem(ACCESS_TOKEN);
        return fetch(ndaURL+'agreements/create_options', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({               
                token : accessToken,             
            })
        }).then((res)=>res.json())
        .catch((error) => {console.error(error);});            
    },
    async deleteNDA(nda){ //.... creating nda 1/2
        let accessToken = await AsyncStorage.getItem(ACCESS_TOKEN);
        return fetch(ndaURL+'agreements/delete', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({               
                token : accessToken, 
                id : nda.id            
            })
        }).then((res)=>res.json())
        .catch((error) => {console.error(error);});            
    },


    async registerDevice(deviceType,deviceToken){ //....storing device token on api
        let accessToken = await AsyncStorage.getItem(ACCESS_TOKEN);
        return fetch(ndaURL + 'devices/register', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({               
                token : accessToken, 
                type : deviceType,            
                device_token : deviceToken          
            })
        }).then((res)=>res.json())
        .catch((error) => {console.error(error);});            
    },
    async resendEmail(ndaID){ //....resend email to invitee
        let accessToken = await AsyncStorage.getItem(ACCESS_TOKEN);
        return fetch(ndaURL + 'agreements/resend', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({               
                token : accessToken, 
                id : ndaID,            
            })
        }).then((res)=>res.json())
        .catch((error) => {console.error(error);});            
    },
    async getNotifications(){ //....get notifications
        let accessToken = await AsyncStorage.getItem(ACCESS_TOKEN);
        return fetch(ndaURL + 'notifications', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({               
                token : accessToken, 
                limit : Globals.notificationsLimit,
                offset : Globals.notificationsOffset       
            })
        }).then((res)=>res.json())
        .catch((error) => {console.error(error);});            
    },
    async deleteNotification(ndaId){ //....get notifications
        let accessToken = await AsyncStorage.getItem(ACCESS_TOKEN);
        return fetch(ndaURL + 'notifications/delete', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({               
                token : accessToken, 
                id : ndaId    
            })
        }).then((res)=>res.json())
        .catch((error) => {console.error(error);});            
    },
    async readNotification(ndaId){ //....get notifications
        let accessToken = await AsyncStorage.getItem(ACCESS_TOKEN);
        return fetch(ndaURL + 'notifications/update', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({               
                token : accessToken, 
                id : ndaId,
                read : "1"    
            })
        }).then((res)=>res.json())
        .catch((error) => {console.error(error);});            
    },
    // async getHTML(encryptedId){ //....get notifications
    //     return fetch(ndaURL + 'agreements?id=' + encryptedId, {
    //         method: 'GET',
    //         headers: {
    //             'Accept': 'application/json',
    //             'Content-Type': 'application/json',
    //         }
    //     }).then((res)=>res.json())
    //     .catch((error) => {console.error(error);});            
    // },
    async getHTML(encryptedId){ //.... getting ndas
        let accessToken = await AsyncStorage.getItem(ACCESS_TOKEN);
        return fetch(ndaURL+'agreements/get_body', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({               
                token : accessToken,  
                id : encryptedId          
            })
        })
            .then((res)=>res.json())
            .catch((error) => {console.error(error);
        });                 
    },
    async createNoticeParty(email){ //....get notifications
        let accessToken = await AsyncStorage.getItem(ACCESS_TOKEN);
        return fetch(ndaURL + 'users/notice_party_create', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({               
                token : accessToken, 
                email : email    
            })
        }).then((res)=>res.json())
        .catch((error) => {console.error(error);});            
    },
    async createNoticePartyWithToken(email,accessToken){ //....get notifications
        console.log("API",accessToken)
        return fetch(ndaURL + 'users/notice_party_create', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({               
                token : accessToken, 
                email : email    
            })
        }).then((res)=>res.json())
        .catch((error) => {console.error(error);});            
    },
    async getNoticeParty(email){ //....get notifications
        let accessToken = await AsyncStorage.getItem(ACCESS_TOKEN);
        return fetch(ndaURL + 'users/notice_party', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({               
                token : accessToken, 
            })
        }).then((res)=>res.json())
        .catch((error) => {console.error(error);});            
    },
    async register(data){ //....get notifications
        return fetch(ndaURL + 'users/register', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({               
                email : data.email,               
                password : data.password,               
                first_name : data.firstName,               
                last_name : data.lastName,               
                company_name : data.companyNmae,
                notice_party : data.noticePartyEmail,               
            })
        }).then((res)=>res.json())
        .catch((error) => {console.error(error);});            
    },
};
module.exports = api;
