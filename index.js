
import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  YellowBox
    } from 'react-native';
import NavigationExperimental from 'react-native-deprecated-custom-components';        
import {TabNavigator} from 'react-navigation';
import {icon} from 'react-native-elements'    ;
import LoginForm from './src/components/LoginForm';
import LandingPage from './src/components/LandingPage';
import {Tabs} from './src/components/router';
import Dashboard from './src/components/Dashboard';
import Splash from './src/components/Splash';
import RegisterForm from './src/components/RegisterForm';
YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader']);

export default class nda extends Component {
constructor(props){
  super(props);
}
  renderScene(route,navigator){
    if (route.name=='LoginForm'){
        return(<LoginForm  navigator={navigator} panHandlers={null}/>)
    }    
    if (route.name=='LandingPage'){
        return(<LandingPage  navigator={navigator} panHandlers={null}/>)
    }
    if (route.name=='Dashboard'){
        return <Tabs />
    }
    if (route.name=='Splash'){
        return<Splash  navigator={navigator}
        panHandlers={null}/>
    } 
    if (route.name=='RegisterForm'){
        return<RegisterForm  navigator={navigator}
        panHandlers={null}/>
    } 
}
  render() {
    return ( 
        <NavigationExperimental.Navigator
            initialRoute={{ name:'Splash'}}
            renderScene={this.renderScene.bind(this)}
        />         
    );
  }
}
AppRegistry.registerComponent('nda', () => nda);